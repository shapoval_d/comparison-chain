﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ComparisonChain.Implementation;
using UnitTests.TestClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Diagnostics;

namespace ComparisonChainFramework.UnitTests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class GeneralTests
    {
        private const string FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE = "First parameter is Null!";

        private const string SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE = "Second parameter is Null!";

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), AllowDerivedTypes = false)]
        public void Comparer_NullFunctor_ArgumentNullExceptionThrown()
        {
            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property<int>(null)
                                        .Build();
        }

        [TestMethod]
        public void Comparer_FirstNull_ComparisonFail()
        {
            // Arrange
            SingleStringClass a1 = null;
            SingleStringClass a2 = new SingleStringClass { String1 = "str1" };

            var cchain = ComparerBuilder.Create<SingleStringClass>().Property(p => p.String1).Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void Comparer_FirstNull_CorrectMessageThrown()
        {
            // Arrange
            SingleStringClass a1 = null;
            SingleStringClass a2 = new SingleStringClass { String1 = "str1" };

            string expectedMessage = FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE;
            string actualMessage = string.Empty;
            var cchain = ComparerBuilder.Create<SingleStringClass>()
                                        .Property(p => p.String1)
                                        .OnCompareFault(d => actualMessage = d.ShowFaults())
                                        .OnCompareFault(d => Debug.WriteLine(actualMessage))
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.IsTrue(actualMessage.Contains(expectedMessage));
        }

        [TestMethod]
        public void Comparer_SecondNull_ComparisonFail()
        {
            // Arrange
            SingleStringClass a1 = new SingleStringClass { String1 = "str1" };
            SingleStringClass a2 = null;

            var cchain = ComparerBuilder.Create<SingleStringClass>().Property(p => p.String1).Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void Comparer_SecondNull_CorrectMessageThrown()
        {
            // Arrange
            SingleStringClass a1 = new SingleStringClass { String1 = "str1" };
            SingleStringClass a2 = null;

            string expectedMessage = SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE;
            string actualMessage = string.Empty;
            var cchain = ComparerBuilder.Create<SingleStringClass>()
                                        .Property(p => p.String1)
                                        .OnCompareFault(d => actualMessage = d.ShowFaults())
                                        .OnCompareFault(d => Debug.WriteLine(actualMessage))
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.IsTrue(actualMessage.Contains(expectedMessage));
        }

        [TestMethod]
        public void Comparer_BothNull_ComparisonSuccess()
        {
            // Arrange
            SingleStringClass a1 = null;
            SingleStringClass a2 = null;

            var cchain = ComparerBuilder.Create<SingleStringClass>()
                                        .Property(p => p.String1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Comparer_FirstParameterDerived_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleDerivedClass { SimpleField2 = 1 };
            var a2 = new SimpleBaseClass();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Comparer_SecondParameterDerived_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass();
            var a2 = new SimpleDerivedClass { SimpleField2 = 1 };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Comparer_BothParametersNullDerived_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleDerivedClass { SimpleField2 = 1 };
            var a2 = new SimpleDerivedClass { SimpleField2 = 2 };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void SeveralPropeties_AllLinksChecked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SeveralStringClass { String1 = "str1", String2 = "str2", String3 = "str3" };
            var a2 = new SeveralStringClass { String1 = "str11", String2 = "str22", String3 = "str33" };

            var customComparer1 = new Mock<IComparer<string>>();
            customComparer1.Setup(c => c.Compare(It.IsAny<string>(), It.IsAny<string>())).Returns(COMPARE_RESULT);
            var customComparer2 = new Mock<IComparer<string>>();
            customComparer2.Setup(c => c.Compare(It.IsAny<string>(), It.IsAny<string>())).Returns(COMPARE_RESULT);
            var customComparer3 = new Mock<IComparer<string>>();
            customComparer3.Setup(c => c.Compare(It.IsAny<string>(), It.IsAny<string>())).Returns(COMPARE_RESULT);

            var cchain = ComparerBuilder.Create<SeveralStringClass>()
                                        .Property(p => p.String1, customComparer1.Object)
                                        .Property(p => p.String2, customComparer2.Object)
                                        .Property(p => p.String3, customComparer3.Object)
                                        .Build();

            // Act
            cchain.Compare(a1, a2);

            // Assert
            customComparer1.Verify(dc => dc.Compare(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            customComparer2.Verify(dc => dc.Compare(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            customComparer3.Verify(dc => dc.Compare(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }
    }
}
