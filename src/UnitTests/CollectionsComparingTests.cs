﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using ComparisonChain.Implementation;
using UnitTests.TestClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ComparisonChainFramework.UnitTests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class CollectionsComparingTests
    {
        private const string DIFFERENT_COUNT_OF_ITEMS_ERROR_MESSAGE = "Different count of items in compared collections!";
        private const string FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE = "First parameter is null!";
        private const string SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE = "Second parameter is null!";

        [TestMethod]
        public void FirstParameterIsNull_ComparisonFail()
        {
            // Arrange
            List<SimpleBaseClass> a1 = null;

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass (),
                    new SimpleBaseClass ()
                };


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void FirstParameterIsNull_CorrectMessageThrown()
        {
            // Arrange
            List<SimpleBaseClass> a1 = null;

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass (),
                    new SimpleBaseClass ()
                };

            string errorMessage = string.Empty;
            string expectedMessage = FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE;
            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);
            var containMessage = errorMessage.Contains(expectedMessage);

            // Assert
            Assert.IsTrue(containMessage);
        }

        [TestMethod]
        public void SecondParameterIsNull_ComparisonFail()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass (),
                    new SimpleBaseClass ()
                };

            List<SimpleBaseClass> a2 = null;


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void SecondParameterIsNull_CorrectMessageThrown()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass (),
                    new SimpleBaseClass ()
                };

            List<SimpleBaseClass> a2 = null;


            string errorMessage = string.Empty;
            string expectedMessage = SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE;
            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);
            var containMessage = errorMessage.Contains(expectedMessage);

            // Assert
            Assert.IsTrue(containMessage);
        }
        
        [TestMethod]
        public void DifferentCountOfItems_ComparisonFail()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass (),
                    new SimpleBaseClass (),
                    new SimpleBaseClass ()
                };

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass (),
                    new SimpleBaseClass ()
                };


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void DifferentCountOfItems_CorrectMessageThrown()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass (),
                    new SimpleBaseClass (),
                    new SimpleBaseClass ()
                };

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass (),
                    new SimpleBaseClass ()
                };

            string errorMessage = string.Empty;
            string expectedMessage = DIFFERENT_COUNT_OF_ITEMS_ERROR_MESSAGE;


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.GetCollectionComparer().Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage);

            // Assert
            Assert.IsTrue(containMessage);
        }

        [TestMethod]
        public void SimplePropertyCheck_ObjectsEqual_ComparisonSuccess()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass {SimpleField1 = 1},
                    new SimpleBaseClass {SimpleField1 = 2},
                    new SimpleBaseClass {SimpleField1 = 3}
                };

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass {SimpleField1 = 1},
                    new SimpleBaseClass {SimpleField1 = 2},
                    new SimpleBaseClass {SimpleField1 = 3}
                };


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void SimplePropertyCheck_ObjectsNotEqual_ComparisonFail()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass {SimpleField1 = 1},
                    new SimpleBaseClass {SimpleField1 = 2},
                    new SimpleBaseClass {SimpleField1 = 3}
                };

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass {SimpleField1 = 1},
                    new SimpleBaseClass {SimpleField1 = 100},
                    new SimpleBaseClass {SimpleField1 = 3}
                };


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void SimplePropertyCheck_ObjectsNotEqual_CorrectErrorMessageThrown()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass {SimpleField1 = 1},
                    new SimpleBaseClass {SimpleField1 = 2},
                    new SimpleBaseClass {SimpleField1 = 3}
                };

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass {SimpleField1 = 1},
                    new SimpleBaseClass {SimpleField1 = 100},
                    new SimpleBaseClass {SimpleField1 = 3}
                };

            string errorMessage = string.Empty;
            string expectedMessage1 = "[1] =>";
            string expectedMessage2 = "SimpleField1";


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.GetCollectionComparer().Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2);

            // Assert
            Assert.IsTrue(containMessage);
        }

        [TestMethod]
        public void EnumPropertyCheck_ObjectsEqual_ComparisonSuccess()
        {
            // Arrange
            var a1 = new List<EnumPropertyClass>
                {
                    new EnumPropertyClass {EnumProperty = TestEnum.First},
                    new EnumPropertyClass {EnumProperty = TestEnum.Second},
                    new EnumPropertyClass {EnumProperty = TestEnum.Third}
                };

            var a2 = new List<EnumPropertyClass>
                {
                    new EnumPropertyClass {EnumProperty = TestEnum.First},
                    new EnumPropertyClass {EnumProperty = TestEnum.Second},
                    new EnumPropertyClass {EnumProperty = TestEnum.Third}
                };


            var cchain = ComparerBuilder.Create<EnumPropertyClass>()
                                        .Property(p => p.EnumProperty)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void EnumPropertyCheck_ObjectsNotEqual_ComparisonFail()
        {
            // Arrange
            var a1 = new List<EnumPropertyClass>
                {
                    new EnumPropertyClass {EnumProperty = TestEnum.First},
                    new EnumPropertyClass {EnumProperty = TestEnum.Second},
                    new EnumPropertyClass {EnumProperty = TestEnum.Third}
                };

            var a2 = new List<EnumPropertyClass>
                {
                    new EnumPropertyClass {EnumProperty = TestEnum.First},
                    new EnumPropertyClass {EnumProperty = TestEnum.Third},
                    new EnumPropertyClass {EnumProperty = TestEnum.Second}
                };


            var cchain = ComparerBuilder.Create<EnumPropertyClass>()
                                        .Property(p => p.EnumProperty)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void NullablePropertyCheck_ObjectsEqual_ComparisonSuccess()
        {
            // Arrange
            var a1 = new List<NullablePropertyClass>
                {
                    new NullablePropertyClass {Int5 = 1},
                    new NullablePropertyClass {Int5 = 2},
                    new NullablePropertyClass {Int5 = 3}
                };

            var a2 = new List<NullablePropertyClass>
                {
                    new NullablePropertyClass {Int5 = 1},
                    new NullablePropertyClass {Int5 = 2},
                    new NullablePropertyClass {Int5 = 3}
                };


            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Int5)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void NullablePropertyCheck_ObjectsNotEqual_ComparisonFail()
        {
            // Arrange
            var a1 = new List<NullablePropertyClass>
                {
                    new NullablePropertyClass {Int5 = 1},
                    new NullablePropertyClass {Int5 = 2},
                    new NullablePropertyClass {Int5 = 3}
                };

            var a2 = new List<NullablePropertyClass>
                {
                    new NullablePropertyClass {Int5 = 1},
                    new NullablePropertyClass {Int5 = 100},
                    new NullablePropertyClass {Int5 = 3}
                };


            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Int5)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void NonSimplePropertyCheck_ObjectsEqual_ComparisonSuccess()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 2 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 3 } }
                };

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 2 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 3 } }
                };


            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .NestedTypeComparer<OtherBaseClass>(inChain)
                                        .Property(p => p.OtherBaseClassField)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void NonSimplePropertyCheck_ObjectsNotEqual_ComparisonFail()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 2 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 3 } }
                };

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 100 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 3 } }
                };


            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .NestedTypeComparer<OtherBaseClass>(inChain)
                                        .Property(p => p.OtherBaseClassField)
                                        .Build();

            // Act
            var actual = cchain.GetCollectionComparer().Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void NonSimplePropertyCheck_ObjectsNotEqual_CorrectErrorMessageThrown()
        {
            // Arrange
            var a1 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 2 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 3 } }
                };

            var a2 = new List<SimpleBaseClass>
                {
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 100 } },
                    new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 3 } }
                };

            string errorMessage = string.Empty;
            string expectedMessage1 = "[1] =>";
            string expectedMessage2 = "OtherBaseClassField";
            string expectedMessage3 = "SimpleField1";


            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .NestedTypeComparer<OtherBaseClass>(inChain)
                                        .Property(p => p.OtherBaseClassField)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.GetCollectionComparer().Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2)
                                  && errorMessage.Contains(expectedMessage3);

            // Assert
            Assert.IsTrue(containMessage);
        }
    }
}
