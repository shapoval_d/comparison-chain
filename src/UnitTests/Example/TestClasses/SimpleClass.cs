﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Examples.TestClasses.Simple
{
    public class Coder
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string HomeAddress { get; set; }
        public int ExperienceMonth { get; set; }
        public string Office { get; set; }
        public string CurrentTeam { get; set; }
        public string[] PreviousTeams { get; set; }
        public string CurrentProject { get; set; }
        public string[] PreviousProjects { get; set; }
    }

    public class SomeCoderComparer : IComparer<Coder>
    {
        public int Compare(Coder x, Coder y)
        {
            int result = 0;

            if (x.Id != y.Id)
            {
                result = 1;
            }

            if (x.FirstName != y.FirstName)
            {
                result = 1;
            }
            
            if (x.SecondName != y.SecondName)
            {
                result = 1;
            }

            if (x.Email != y.Email)
            {
                result = 1;
            }

            if (x.HomeAddress != y.HomeAddress)
            {
                result = 1;
            }

            if (x.ExperienceMonth != y.ExperienceMonth)
            {
                result = 1;
            }

            if (x.Office != y.Office)
            {
                result = 1;
            }

            if (x.CurrentTeam != y.CurrentTeam)
            {
                result = 1;
            }

            if (x.PreviousTeams.Length != y.PreviousTeams.Length)
            {
                result = 1;
            }
            else
            {
                for (int i = 0; i < x.PreviousTeams.Length; i++)
                {
                    if (x.PreviousTeams[i] != y.PreviousTeams[i])
                    {
                        result = 1;
                    }
                }
            }
            
            if (x.CurrentProject != y.CurrentProject)
            {
                result = 1;
            }

            if (x.PreviousProjects.Length != y.PreviousProjects.Length)
            {
                result = 1;
            }
            else
            {
                for (int i = 0; i < x.PreviousProjects.Length; i++)
                {
                    if (x.PreviousProjects[i] != y.PreviousProjects[i])
                    {
                        result = 1;
                    }
                }
            }

            return result;
        }
    }

    public class AnotherCoderComparer : IComparer<Coder>
    {
        public int Compare(Coder x, Coder y)
        {
            int result = 0;

            if (x.Id != y.Id)
            {
                result = 1;
                Debug.WriteLine("Id");
            }

            if (x.FirstName != y.FirstName)
            {
                result = 1;
                Debug.WriteLine("FirstName");
            }

            if (x.SecondName != y.SecondName)
            {
                result = 1;
                Debug.WriteLine("SecondName");
            }

            if (x.Email != y.Email)
            {
                result = 1;
                Debug.WriteLine("Email");
            }

            if (x.HomeAddress != y.HomeAddress)
            {
                result = 1;
                Debug.WriteLine("HomeAddress");
            }

            if (x.ExperienceMonth != y.ExperienceMonth)
            {
                result = 1;
                Debug.WriteLine("ExperienceMonth");
            }

            if (x.Office != y.Office)
            {
                result = 1;
                Debug.WriteLine("Office");
            }

            if (x.CurrentTeam != y.CurrentTeam)
            {
                result = 1;
                Debug.WriteLine("CurrentTeam");
            }

            if (x.PreviousTeams.Length != y.PreviousTeams.Length)
            {
                result = 1;
                Debug.WriteLine("PreviousTeams.Length");
            }
            else
            {
                for (int i = 0; i < x.PreviousTeams.Length; i++)
                {
                    if (x.PreviousTeams[i] != y.PreviousTeams[i])
                    {
                        result = 1;
                        Debug.WriteLine(string.Format("PreviousTeams[{0}]", i.ToString()));
                    }
                }
            }

            if (x.CurrentProject != y.CurrentProject)
            {
                result = 1;
                Debug.WriteLine("CurrentProject");
            }

            if (x.PreviousProjects.Length != y.PreviousProjects.Length)
            {
                result = 1;
                Debug.WriteLine("PreviousProjects.Length");
            }
            else
            {
                for (int i = 0; i < x.PreviousProjects.Length; i++)
                {
                    if (x.PreviousProjects[i] != y.PreviousProjects[i])
                    {
                        result = 1;
                        Debug.WriteLine(string.Format("PreviousProjects[{0}]", i.ToString()));
                    }
                }
            }

            return result;
        }
    }
}
