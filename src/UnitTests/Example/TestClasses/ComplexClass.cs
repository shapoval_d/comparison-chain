﻿using System;
using System.Collections.Generic;

namespace Examples.TestClasses.Complex
{
    #region Aggregated classes

    public class Address
    {
        public string Country { get; set; }
        public string CityName { get; set; }
        public int PostalCode { get; set; }
        public string DistrictName { get; set; }
        public string StreetName { get; set; }
        public string HouseNumber { get; set; }
    }

    public class Person
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Address HomeAddress { get; set; }
    }

    public class Office
    {
        public string Name { get; set; }
        public Address OfficeAddress { get; set; }
    }

    public class Team
    {
        public string Name { get; set; }
        public Project Project { get; set; }
    }

    public class Project
    {
        public string Name { get; set; }
        public string Delievery { get; set; }
        public string Customer { get; set; }
    }

    #endregion

    public class HardCoder
    {
        public int Id { get; set; }
        public Person PersonInfo { get; set; }
        public int ExperienceMonth { get; set; }
        public Office Office { get; set; }
        public Team CurrentTeam { get; set; }
        public List<Team> PreviousTeams { get; set; }
    }


    public class HardCoderAccordinglyHardComparer : IComparer<HardCoder>
    {
        public int Compare(HardCoder x, HardCoder y)
        {
            int result = 0;

            if (x.Id != y.Id)
            {
                result = 1;
            }

            if (x.ExperienceMonth != y.ExperienceMonth)
            {
                result = 1;
            }

            int innerResult = CheckPerson(x.PersonInfo, y.PersonInfo);
            if (innerResult != 0)
            {
                result = innerResult;
            }

            innerResult = CheckOffice(x.Office, y.Office);
            if (innerResult != 0)
            {
                result = innerResult;
            }

            innerResult = CheckTeam(x.CurrentTeam, y.CurrentTeam);
            if (innerResult != 0)
            {
                result = innerResult;
            }

            if (x.PreviousTeams != null && y.PreviousTeams != null)
            {
                if (x.PreviousTeams.Count != y.PreviousTeams.Count)
                {
                    result = 1;
                }
                else
                {
                    for (int i = 0; i < x.PreviousTeams.Count; i++)
                    {
                        innerResult = CheckTeam(x.PreviousTeams[i], y.PreviousTeams[i]);
                        if (innerResult != 0)
                        {
                            result = innerResult;
                        }
                    }
                }
            }
            else if (x.PreviousTeams == null && y.PreviousTeams != null)
            {
                result = 1;
            }
            else if (x.PreviousTeams != null && y.PreviousTeams == null)
            {
                result = 1;
            }

            return result;
        }

        private int CheckPerson(Person x, Person y)
        {
            int result = 0;

            if (x != null && y != null)
            {
                if (x.FirstName != y.FirstName)
                {
                    result = 1;
                }

                if (x.SecondName != y.SecondName)
                {
                    result = 1;
                }

                if (x.Email != y.Email)
                {
                    result = 1;
                }

                if (x.Phone != y.Phone)
                {
                    result = 1;
                }

                int innerResult = CheckAdress(x.HomeAddress, y.HomeAddress);
                if (innerResult != 0)
                {
                    result = innerResult;
                }
            }
            else if (x == null && y != null)
            {
                result = 1;
            }
            else if (x != null && y == null)
            {
                result = 1;
            }

            return result;
        }

        private int CheckOffice(Office x, Office y)
        {
            int result = 0;

            if (x != null && x != null)
            {
                if (x.Name != y.Name)
                {
                    result = 1;
                }

                if (x.OfficeAddress != null && y.OfficeAddress != null)
                {
                    int innerResult = CheckAdress(x.OfficeAddress, y.OfficeAddress);
                    if (innerResult != 0)
                    {
                        result = innerResult;
                    }
                }
            }
            else if (x == null && y != null)
            {
                result = 1;
            }
            else if (x != null && y == null)
            {
                result = 1;
            }

            return result;
        }

        private int CheckAdress(Address x, Address y)
        {
            int result = 0;
            if (x != null && y != null)
            {
                if (x.Country != y.Country)
                {
                    result = 1;
                };

                if (x.CityName != y.CityName)
                {
                    result = 1;
                };

                if (x.DistrictName != y.DistrictName)
                {
                    result = 1;
                };

                if (x.HouseNumber != y.HouseNumber)
                {
                    result = 1;
                };
            }
            else if (x == null && y != null)
            {
                result = 1;
            }
            else if (x != null && y == null)
            {
                result = 1;
            }

            return result;
        }

        private int CheckTeam(Team x, Team y)
        {
            int result = 0;
            if (x != null && y != null)
            {
                if (x.Name != y.Name)
                {
                    result = 1;
                };

                int innerResult = CheckProject(x.Project, y.Project);
                if (innerResult != 0)
                {
                    result = innerResult;
                }
            }
            else if (x == null && y != null)
            {
                result = 1;
            }
            else if (x != null && y == null)
            {

            }

            return result;
        }

        private int CheckProject(Project x, Project y)
        {
            // Some same code
            return 0;
        }
    }

    public class HardCoderLuckyGuysComparer : IComparer<HardCoder>
    {
        private IComparer<Person> PersonComparer;
        private IComparer<Office> OfficeComparer;
        private IComparer<Address> AddressComparer;
        private IComparer<Team> TeamComparer;
        private IComparer<Project> ProjectComparer;

        public int Compare(HardCoder x, HardCoder y)
        {
            // I think you know what to do :);

            return 0;
        }
    }
}
