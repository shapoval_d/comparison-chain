﻿using System.Collections.Generic;
using System.Diagnostics;
using ComparisonChain.Implementation;
using Examples.TestClasses.Complex;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ComparisonChainExamples
{
    [TestClass]
    public class Example
    {
        private readonly IComparer<HardCoder> _hardCoderComparer = CreateComparer();

        [TestMethod]
        //[ExpectedException(typeof(AssertFailedException))]
        public void OutputExample()
        {          
            // Arrange
            HardCoder expected = GetExpected();

            // Act
            HardCoder actual = GetActual();

            //Assert
            Assert.AreEqual(0, _hardCoderComparer.Compare(expected, actual));
        }

        [TestMethod]
        public void CustomInnerComparer()
        {
            // Arrange
            IComparer<HardCoder> hardCoderComparer = CreateComparerWithCustomIntComparer();
            HardCoder expected = GetExpected();

            // Act
            HardCoder actual = GetActual();

            //Assert
            Assert.AreEqual(0, hardCoderComparer.Compare(expected, actual));
        }

        private static IComparer<HardCoder> CreateComparer()
        {
            var addressComparer = ComparerBuilder.Create<Address>()
                                                 .Property(p => p.CityName)
                                                 .Property(p => p.Country)
                                                 .Property(p => p.DistrictName)
                                                 .Property(p => p.HouseNumber)
                                                 .Property(p => p.PostalCode)
                                                 .Property(p => p.StreetName)
                                                 .Build();

            var personComparer = ComparerBuilder.Create<Person>()
                                                .Property(p => p.Email)
                                                .Property(p => p.FirstName)
                                                .Property(p => p.HomeAddress, addressComparer)
                                                .Property(p => p.Phone)
                                                .Property(p => p.SecondName)
                                                .Build();

            var officeComparer = ComparerBuilder.Create<Office>()
                                                .Property(p => p.Name)
                                                .Property(p => p.OfficeAddress, addressComparer)
                                                .Build();

            var projectComparer = ComparerBuilder.Create<Project>()
                                                 .Property(p => p.Name)
                                                 .Property(p => p.Customer)
                                                 .Property(p => p.Delievery)
                                                 .Build();

            var teamComparer = ComparerBuilder.Create<Team>()
                                              .Property(p => p.Name)
                                              .Property(p => p.Project, projectComparer)
                                              .Build();

            return ComparerBuilder.Create<HardCoder>()
                                  .Property(p => p.PersonInfo, personComparer)
                                  .Property(p => p.Office, officeComparer)
                                  .Property(p => p.CurrentTeam, teamComparer)
                                  .Property(p => p.Id)
                                  .Property(p => p.ExperienceMonth)
                                  .PropertyCollection(p => p.PreviousTeams, teamComparer)
                                  .OnCompareFault(cr => Debug.WriteLine(cr.ShowFaults()))
                                  //.OnCompareFault(cr => { throw new AssertFailedException(cr.ShowFaults());})
                                  .Build();
        }

        private static ComparisonChain<HardCoder> CreateComparerWithCustomIntComparer()
        {

            var addressComparer = ComparerBuilder.Create<Address>()
                                                 .Property(p => p.CityName)
                                                 .Property(p => p.Country)
                                                 .Property(p => p.DistrictName)
                                                 .Property(p => p.HouseNumber)
                                                 .Property(p => p.PostalCode, new CustomPostalCodeComparer())
                                                 .Property(p => p.StreetName)
                                                 .Build();

            var personComparer = ComparerBuilder.Create<Person>()
                                                .Property(p => p.Email)
                                                .Property(p => p.FirstName)
                                                .Property(p => p.HomeAddress)
                                                .Property(p => p.Phone)
                                                .Property(p => p.SecondName)
                                                .Build();

            var officeComparer = ComparerBuilder.Create<Office>()
                                                .Property(p => p.Name)
                                                .Property(p => p.OfficeAddress)
                                                .Build();

            var projectComparer = ComparerBuilder.Create<Project>()
                                                 .Property(p => p.Name)
                                                 .Property(p => p.Customer)
                                                 .Property(p => p.Delievery)
                                                 .Build();

            var teamComparer = ComparerBuilder.Create<Team>()
                                              .Property(p => p.Name)
                                              .Property(p => p.Project, projectComparer)
                                              .Build();

            return ComparerBuilder.Create<HardCoder>()
                                  .Property(p => p.PersonInfo, personComparer)
                                  .Property(p => p.Office, officeComparer)
                                  .Property(p => p.CurrentTeam, teamComparer)
                                  .Property(p => p.Id)
                                  .Property(p => p.ExperienceMonth)
                                  .PropertyCollection(p => p.PreviousTeams, teamComparer)
                                  .OnCompareFault(cr => Debug.WriteLine(cr.ShowFaults()))
                                  .Build();
        }

        private static HardCoder GetExpected()
        {
            return new HardCoder
            {
                Id = 1,
                ExperienceMonth = 10,
                PersonInfo = new Person
                {
                    FirstName = "FirstName",
                    Email = "email@softserveinc.com",
                    HomeAddress = new Address
                    {
                        Country = "Ukraine",
                        CityName = "Dnipropetrovsk",
                        DistrictName = "DistrictName",
                        PostalCode = 49000,
                        StreetName = "StreetName",
                        HouseNumber = "10/2"
                    }
                },
                CurrentTeam = new Team
                {
                    Name = "TeamName",
                    Project = new Project
                    {
                        Name = "ProjectName",
                        Delievery = "DeliveryName",
                        Customer = "SomeCustomer"
                    },
                },
                Office = new Office
                {
                    Name = "Dnipro1",
                    OfficeAddress = null
                },
                PreviousTeams = new List<Team>
                                {
                                    new Team { Name = "SomeTeam1" },
                                    new Team { Name = "SomeTeam2" },
                                    new Team { Name = "SomeTeam3" },
                                    new Team { Name = "SomeTeam4" },
                                }
            };
        }

        private static HardCoder GetActual()
        {
            return new HardCoder
            {
                Id = 1,
                ExperienceMonth = 12,
                PersonInfo = new Person
                {
                    FirstName = "FirstName",
                    Email = "email@softserve.com",
                    HomeAddress = new Address
                    {
                        Country = "Ukraine",
                        CityName = "Dnipropetrovsk",
                        DistrictName = null,
                        PostalCode = 49066,
                        StreetName = "StreetName",
                        HouseNumber = "10/2a"
                    }
                },
                CurrentTeam = new Team
                {
                    Name = "TeamName",
                    Project = new Project
                    {
                        Name = "AnotherProjectName",
                        Delievery = "DelieveryName",
                        Customer = "AnotherSomeCustomer"
                    }
                },
                Office = new Office
                {
                    Name = "Dnipro2",
                    OfficeAddress = new Address
                    {
                        CityName = "Dnipropetrovsk"
                    }
                },
                PreviousTeams = new List<Team>
                                {
                                    new Team { Name = "SomeTeam1" },
                                    new Team { Name = "SomeTeam2" },
                                    new Team { Name = "SomeTeam4" },
                                    new Team { Name = "SomeTeam5" },
                                }
            };
        }

        private class CustomPostalCodeComparer : IComparer<int>
        {
            public int Compare(int x, int y)
            {
                int result = 0;
                string mainPostalCodeX = x.ToString().Substring(0, 3);
                string mainPostalCodeY = x.ToString().Substring(0, 3);

                if (mainPostalCodeX != mainPostalCodeY)
                {
                    result = 1;
                }

                return result;
            }
        }
    }
}
