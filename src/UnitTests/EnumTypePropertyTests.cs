﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation;
using UnitTests.TestClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ComparisonChainFramework.UnitTests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class EnumTypePropertyTests
    {
        [TestMethod]
        public void ComparerSupplied_SpecifiedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new EnumPropertyClass { EnumProperty = TestEnum.First };
            var a2 = new EnumPropertyClass { EnumProperty = TestEnum.Second };

            var customComparer = new Mock<IComparer<TestEnum>>();

            customComparer.Setup(c => c.Compare(It.IsAny<TestEnum>(), It.IsAny<TestEnum>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<EnumPropertyClass>()
                                       .Property(p => p.EnumProperty, customComparer.Object)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<TestEnum>(), It.IsAny<TestEnum>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void NoComparerSupplied_DefaultComparerFound()
        {
            // Should divide it into two methods?

            // Arrange
            const int COMPARE_RESULT = 0;
            var mockCompareResult = new Mock<ICompareResult>();
            mockCompareResult.Setup(c => c.Result).Returns(COMPARE_RESULT);

            var a1 = new EnumPropertyClass { EnumProperty = TestEnum.First };
            var a2 = new EnumPropertyClass { EnumProperty = TestEnum.Second };

            var customDefaultComparerProvider = new Mock<IComparerProvider>();
            var customEnumComparer = new Mock<IComparerWrapper<TestEnum>>();

            customDefaultComparerProvider.Setup(dc => dc.GetComparer<TestEnum>(null)).Returns(customEnumComparer.Object);
            customEnumComparer.Setup(c => c.Compare(It.IsAny<TestEnum>(), It.IsAny<TestEnum>())).Returns(mockCompareResult.Object);

            // Act
            var chain = ComparerBuilder.Create<EnumPropertyClass>(customDefaultComparerProvider.Object)
                                       .Property(p => p.EnumProperty)
                                       .Build();

            var actual = chain.Compare(a1, a2);

            // Assert
            customDefaultComparerProvider.Verify(dc => dc.GetComparer<TestEnum>(null), Times.Once);
            customEnumComparer.Verify(dc => dc.Compare(It.IsAny<TestEnum>(), It.IsAny<TestEnum>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void EqualObjects_ComparisonSuccess()
        {
            // Arrange
            var a1 = new EnumPropertyClass { EnumProperty = TestEnum.First};
            var a2 = new EnumPropertyClass { EnumProperty = TestEnum.First };

            var cchain = ComparerBuilder.Create<EnumPropertyClass>()
                                        .Property(p => p.EnumProperty)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void NotEqualObjects_ComparisonFail()
        {
            // Arrange
            var a1 = new EnumPropertyClass { EnumProperty = TestEnum.First };
            var a2 = new EnumPropertyClass { EnumProperty = TestEnum.Second };

            var cchain = ComparerBuilder.Create<EnumPropertyClass>()
                                        .Property(p => p.EnumProperty)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void NotEqualObjects_CorrectMessageThrown()
        {
            // Arrange
            var a1 = new EnumPropertyClass { EnumProperty = TestEnum.First };
            var a2 = new EnumPropertyClass { EnumProperty = TestEnum.Second };

            string expectedMessage = "EnumProperty";
            string actualMessage = string.Empty;
            var cchain = ComparerBuilder.Create<EnumPropertyClass>()
                                        .Property(p => p.EnumProperty)
                                        //.OnCompareFault(d => actualMessage = d.ShowFaults())
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.IsTrue(actualMessage.Contains(expectedMessage));
        }
    }
}
