﻿using System.Diagnostics.CodeAnalysis;

namespace UnitTests.TestClasses
{
    [ExcludeFromCodeCoverage]
    public class SimpleTypesClass
    {
        public byte Byte1 { get; set; }

        public sbyte SByte2 { get; set; }

        public short Short3 { get; set; }

        public ushort UShort4 { get; set; }

        public int Int5 { get; set; }

        public uint UInt6 { get; set; }

        public long Long7 { get; set; }

        public ulong ULong8 { get; set; }

        public float Float9 { get; set; }

        public double Double10 { get; set; }

        public decimal Decimal11 { get; set; }

        public bool Bool12 { get; set; }

        public char Char13 { get; set; }
    }
}
