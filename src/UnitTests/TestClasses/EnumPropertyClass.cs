﻿using System.Diagnostics.CodeAnalysis;

namespace UnitTests.TestClasses
{
    [ExcludeFromCodeCoverage]
    public class EnumPropertyClass
    {
        public TestEnum EnumProperty;
    }

    public enum TestEnum
    {
        First,
        Second,
        Third
    }
}
