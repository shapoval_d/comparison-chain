﻿using System.Diagnostics.CodeAnalysis;

namespace UnitTests.TestClasses
{
    [ExcludeFromCodeCoverage]
    public class SimpleDerivedClass : SimpleBaseClass
    {
        public int SimpleField2 { get; set; }
    }
}
