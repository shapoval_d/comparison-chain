﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace UnitTests.TestClasses
{
    [ExcludeFromCodeCoverage]
    public class SimpleBaseClass
    {
        public int SimpleField1 { get; set; }
        public IEnumerable<int> SimpleCollectionField { get; set; }
        public int[] SimpleArrayField { get; set; }


        public SimpleBaseClass SameClassField { get; set; }
        public SimpleDerivedClass DerivedFromSameClassField { get; set; }
        public IEnumerable<SimpleBaseClass> SameClassCollectionField { get; set; }


        public OtherBaseClass OtherBaseClassField { get; set; }
        public OtherDerivedClass OtherDerivedClassField { get; set; }
        public IEnumerable<OtherBaseClass> OtherClassCollectionField { get; set; }

    }
}
