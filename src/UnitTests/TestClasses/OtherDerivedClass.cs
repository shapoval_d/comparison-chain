﻿using System.Diagnostics.CodeAnalysis;

namespace UnitTests.TestClasses
{
    [ExcludeFromCodeCoverage]
    public class OtherDerivedClass : OtherBaseClass
    {
        public int SimpleField2 { get; set; }
    }
}
