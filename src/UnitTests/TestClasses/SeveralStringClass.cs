﻿using System.Diagnostics.CodeAnalysis;

namespace UnitTests.TestClasses
{
    [ExcludeFromCodeCoverage]
    public class SeveralStringClass
    {
        public string String1 { get; set; }

        public string String2 { get; set; }

        public string String3 { get; set; }
    }
}
