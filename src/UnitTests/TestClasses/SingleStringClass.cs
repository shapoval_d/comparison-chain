﻿using System.Diagnostics.CodeAnalysis;

namespace UnitTests.TestClasses
{
    [ExcludeFromCodeCoverage]
    public class SingleStringClass
    {
        public string String1 { get; set; }
    }
}
