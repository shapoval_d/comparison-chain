﻿using System.Diagnostics.CodeAnalysis;

namespace UnitTests.TestClasses
{
    [ExcludeFromCodeCoverage]
    public class OtherBaseClass
    {
        public int SimpleField1 { get; set; }
    }
}
