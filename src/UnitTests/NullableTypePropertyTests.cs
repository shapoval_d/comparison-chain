﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation;
using UnitTests.TestClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Diagnostics;

namespace ComparisonChainFramework.UnitTests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class NullableTypePropertyTests
    {
        private const string FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE = "First parameter is Null!";

        private const string SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE = "Second parameter is Null!";

        [TestMethod]
        public void ComparerSupplied_SpecifiedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new NullablePropertyClass { Int5 = 1 };
            var a2 = new NullablePropertyClass { Int5 = 5 };

            var customComparer = new Mock<IComparer<int?>>();

            customComparer.Setup(c => c.Compare(It.IsAny<int?>(), It.IsAny<int?>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<NullablePropertyClass>()
                                       .Property(p => p.Int5, customComparer.Object)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<int?>(), It.IsAny<int?>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void NoComparerSupplied_DefaultComparerFound()
        {
            // Should divide it into two methods?

            // Arrange
            const int COMPARE_RESULT = 0;
            var mockCompareResult = new Mock<ICompareResult>();
            mockCompareResult.Setup(c => c.Result).Returns(COMPARE_RESULT);

            var a1 = new NullablePropertyClass { Int5 = 1 };
            var a2 = new NullablePropertyClass { Int5 = 5 };

            var customDefaultComparerProvider = new Mock<IComparerProvider>();
            var customIntComparer = new Mock<IComparerWrapper<int?>>();

            customDefaultComparerProvider.Setup(dc => dc.GetComparer<int?>(null)).Returns(customIntComparer.Object);
            customIntComparer.Setup(c => c.Compare(It.IsAny<int?>(), It.IsAny<int?>())).Returns(mockCompareResult.Object);

            // Act
            var chain = ComparerBuilder.Create<NullablePropertyClass>(customDefaultComparerProvider.Object)
                                       .Property(p => p.Int5)
                                       .Build();

            var actual = chain.Compare(a1, a2);

            // Assert
            customDefaultComparerProvider.Verify(dc => dc.GetComparer<int?>(null), Times.Once);
            customIntComparer.Verify(dc => dc.Compare(It.IsAny<int?>(), It.IsAny<int?>()), Times.Once);
            Assert.AreEqual(0, actual);
        }
        
        [TestMethod]
        public void PropertyOfFirstIsNull_ComparisonFail()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Int5 = null };
            var a2 = new NullablePropertyClass { Int5 = 14 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Int5)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void PropertyOfFirstIsNull_CorrectMessageThrown()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Int5 = null };
            var a2 = new NullablePropertyClass { Int5 = 14 };

            string expectedMessage1 = FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE;
            string expectedMessage2 = "Int5";
            string actualMessage = string.Empty;
            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Int5)
                                        .OnCompareFault(d => actualMessage = d.ShowFaults())
                                        .OnCompareFault(d => Debug.WriteLine(actualMessage))
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.IsTrue(actualMessage.Contains(expectedMessage1)
                            && actualMessage.Contains(expectedMessage2));
        }

        [TestMethod]
        public void PropertyOfSecondIsNull_ComparisonFail()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Int5 = 14 };
            var a2 = new NullablePropertyClass { Int5 = null };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Int5)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void PropertyOfSecondIsNull_CorrectMessageThrown()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Int5 = 14 };
            var a2 = new NullablePropertyClass { Int5 = null };

            string expectedMessage1 = SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE;
            string expectedMessage2 = "Int5";
            string actualMessage = string.Empty;
            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Int5)
                                        .OnCompareFault(d => actualMessage = d.ShowFaults())
                                        .OnCompareFault(d => Debug.WriteLine(actualMessage))
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.IsTrue(actualMessage.Contains(expectedMessage1)
                            && actualMessage.Contains(expectedMessage2));
        }

        [TestMethod]
        public void PropertyOfBothAreNull_ComparisonSuccess()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Int5 = null };
            var a2 = new NullablePropertyClass { Int5 = null };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Int5)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Byte_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Byte1 = 10 };
            var a2 = new NullablePropertyClass { Byte1 = 10 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Byte1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void SByte_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { SByte2 = 11 };
            var a2 = new NullablePropertyClass { SByte2 = 11 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.SByte2)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Short_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Short3 = 12 };
            var a2 = new NullablePropertyClass { Short3 = 12 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Short3)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void UShort_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { UShort4 = 13 };
            var a2 = new NullablePropertyClass { UShort4 = 13 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.UShort4)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Int_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Int5 = 14 };
            var a2 = new NullablePropertyClass { Int5 = 14 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Int5)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void UInt_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { UInt6 = 15 };
            var a2 = new NullablePropertyClass { UInt6 = 15 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.UInt6)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Long_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Long7 = 16 };
            var a2 = new NullablePropertyClass { Long7 = 16 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Long7)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void ULong_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { ULong8 = 17 };
            var a2 = new NullablePropertyClass { ULong8 = 17 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.ULong8)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Float_CorrectlyCompares()
        {
            // TODO: Add cases for comparing with delta
            // Arrange
            var a1 = new NullablePropertyClass { Float9 = 18f };
            var a2 = new NullablePropertyClass { Float9 = 18f };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Float9)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Double_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Double10 = 19 };
            var a2 = new NullablePropertyClass { Double10 = 19 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Double10)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Decimal_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Decimal11 = 20 };
            var a2 = new NullablePropertyClass { Decimal11 = 20 };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Decimal11)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Bool_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Bool12 = true };
            var a2 = new NullablePropertyClass { Bool12 = true };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Bool12)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Char_CorrectlyCompares()
        {
            // Arrange
            var a1 = new NullablePropertyClass { Char13 = 'c' };
            var a2 = new NullablePropertyClass { Char13 = 'c' };

            var cchain = ComparerBuilder.Create<NullablePropertyClass>()
                                        .Property(p => p.Char13)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }
    }
}
