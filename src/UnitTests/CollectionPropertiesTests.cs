﻿using System.Collections.Generic;
using System.Diagnostics;
using ComparisonChain.Implementation;
using UnitTests.TestClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ComparisonChainFramework.UnitTests
{
    [TestClass]
    public class CollectionPropertiesTests
    {
        private const string DIFFERENT_COUNT_OF_ITEMS_ERROR_MESSAGE = "Different count of items in compared collections!";
        private const string FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE = "First parameter is null!";
        private const string SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE = "Second parameter is null!";
        
        #region Basic checks

        [TestMethod]
        public void PropertyOfFirstIsNull_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SimpleCollectionField = null };
            var a2 = new SimpleBaseClass { SimpleCollectionField = new List<int>() };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SimpleCollectionField)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void PropertyOfSecondIsNull_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SimpleCollectionField = new List<int>() };
            var a2 = new SimpleBaseClass { SimpleCollectionField = null };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SimpleCollectionField)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void PropertyOfFirstIsNull_CorrectErrorMessageThrown()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SimpleCollectionField = null };
            var a2 = new SimpleBaseClass { SimpleCollectionField = new List<int>() };

            string errorMessage = string.Empty;
            string expectedMessage1 = "SimpleCollectionField";
            string expectedMessage2 = FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE;

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SimpleCollectionField)
                                        //.OnCompareFault(c => errorMessage = c.ShowFaults())
                                        //.OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2);

            // Assert
            Assert.IsTrue(containMessage);
        }

        [TestMethod]
        [Ignore]
        public void PropertyOfSecondIsNull_CorrectErrorMessageThrown()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SimpleCollectionField = new List<int>() };
            var a2 = new SimpleBaseClass { SimpleCollectionField = null };

            string errorMessage = string.Empty;
            string expectedMessage1 = "SimpleCollectionField";
            string expectedMessage2 = SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE;

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SimpleCollectionField)
                                        //.OnCompareFault(c => errorMessage = c.ShowFaults())
                                        //.OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2);

            // Assert
            Assert.IsTrue(containMessage);
        }

        [TestMethod]
        public void NonSimpleTypeCollectionProperty_DifferentCountOfItems_ComparisonFail()
        {
            // Arrange
            var nestedCollection1 = new List<OtherBaseClass>
            {
                new OtherBaseClass(),
                new OtherBaseClass(),
                new OtherBaseClass()
            };

            var nestedCollection2 = new List<OtherBaseClass>
            {
                new OtherBaseClass(),
                new OtherBaseClass(),
            };

            var a1 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection2 };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.OtherClassCollectionField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void NonSimpleTypeCollectionProperty_DifferentCountOfItems_CorrectErrorMessageThrown()
        {
            // Arrange
            var nestedCollection1 = new List<OtherBaseClass>
            {
                new OtherBaseClass(),
                new OtherBaseClass(),
                new OtherBaseClass()
            };

            var nestedCollection2 = new List<OtherBaseClass>
            {
                new OtherBaseClass(),
                new OtherBaseClass(),
            };

            var a1 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection2 };

            string errorMessage = string.Empty;
            string expectedMessage1 = "OtherClassCollectionField";
            string expectedMessage2 = DIFFERENT_COUNT_OF_ITEMS_ERROR_MESSAGE;

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.OtherClassCollectionField, inChain)
                                        //.OnCompareFault(c => errorMessage = c.ShowFaults())
                                        //.OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2);

            // Assert
            Assert.IsTrue(containMessage);
        }

        #endregion

        [TestMethod]
        public void SimpleType_ObjectsEqual_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SimpleCollectionField = new List<int> { 1, 2, 3 } };
            var a2 = new SimpleBaseClass { SimpleCollectionField = new List<int> { 1, 2, 3 } };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SimpleCollectionField)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void SimpleType_ObjectsNotEqual_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SimpleCollectionField = new List<int> { 1, 2, 3 } };
            var a2 = new SimpleBaseClass { SimpleCollectionField = new List<int> { 1, 100, 3 } };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SimpleCollectionField)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void SimpleType_ObjectsNotEqual_CorrectErrorMessageThrown()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SimpleCollectionField = new List<int> { 1, 2, 3 } };
            var a2 = new SimpleBaseClass { SimpleCollectionField = new List<int> { 1, 100, 3 } };

            string errorMessage = string.Empty;
            string expectedMessage1 = "SimpleCollectionField";
            string expectedMessage2 = "[1] =>";

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SimpleCollectionField)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .OnCompareFault(c => Debug.WriteLine(errorMessage))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2);

            // Assert
            Assert.IsTrue(containMessage);
        }


        [TestMethod]
        [Ignore]
        public void NonSimpleType_ObjectsEqual_ComparisonSuccess()
        {
            // Arrange
            var nestedCollection1 = new List<OtherBaseClass>
            {
                new OtherBaseClass { SimpleField1 = 1  },
                new OtherBaseClass { SimpleField1 = 2  },
                new OtherBaseClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<OtherBaseClass>
            {
                new OtherBaseClass { SimpleField1 = 1  },
                new OtherBaseClass { SimpleField1 = 2  },
                new OtherBaseClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection2 };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.OtherClassCollectionField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void NonSimpleType_ObjectsNotEqual_ComparisonFail()
        {
            // Arrange
            var nestedCollection1 = new List<OtherBaseClass>
            {
                new OtherBaseClass { SimpleField1 = 1  },
                new OtherBaseClass { SimpleField1 = 2  },
                new OtherBaseClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<OtherBaseClass>
            {
                new OtherBaseClass { SimpleField1 = 1  },
                new OtherBaseClass { SimpleField1 = 100  },
                new OtherBaseClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection2 };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.OtherClassCollectionField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }


        [TestMethod]
        public void NonSimpleType_ObjectsNotEqual_CorrectErrorMessageThrown()
        {
            // Arrange
            var nestedCollection1 = new List<OtherBaseClass>
            {
                new OtherBaseClass { SimpleField1 = 1  },
                new OtherBaseClass { SimpleField1 = 2  },
                new OtherBaseClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<OtherBaseClass>
            {
                new OtherBaseClass { SimpleField1 = 1  },
                new OtherBaseClass { SimpleField1 = 100  },
                new OtherBaseClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection2 };

            string errorMessage = string.Empty;
            string expectedMessage1 = "[1] =>";
            string expectedMessage2 = "OtherClassCollectionField";
            string expectedMessage3 = "SimpleField1";

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.OtherClassCollectionField, inChain)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .OnCompareFault(c => Debug.WriteLine(errorMessage))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2)
                                  && errorMessage.Contains(expectedMessage3);

            // Assert
            Assert.IsTrue(containMessage);
        }

        [TestMethod]
        [Ignore]
        public void NonSimpleType_DerivedTypeCollectionAssigned_ObjectsEqual_ComparisonSuccess()
        {
            // Arrange
            var nestedCollection1 = new List<OtherDerivedClass>
            {
                new OtherDerivedClass { SimpleField1 = 1  },
                new OtherDerivedClass { SimpleField1 = 2  },
                new OtherDerivedClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<OtherDerivedClass>
            {
                new OtherDerivedClass { SimpleField1 = 1  },
                new OtherDerivedClass { SimpleField1 = 2  },
                new OtherDerivedClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection2 };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.OtherClassCollectionField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void NonSimpleType_DerivedTypeCollectionAssigned_ObjectsNotEqual_ComparisonFail()
        {
            // Arrange
            var nestedCollection1 = new List<OtherDerivedClass>
            {
                new OtherDerivedClass { SimpleField1 = 1  },
                new OtherDerivedClass { SimpleField1 = 2  },
                new OtherDerivedClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<OtherDerivedClass>
            {
                new OtherDerivedClass { SimpleField1 = 1  },
                new OtherDerivedClass { SimpleField1 = 100 },
                new OtherDerivedClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection2 };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.OtherClassCollectionField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void NonSimpleType_DerivedTypeCollectionAssigned_ObjectsNotEqual_CorrectErrorMessageThrown()
        {
            // Arrange
            var nestedCollection1 = new List<OtherDerivedClass>
            {
                new OtherDerivedClass { SimpleField1 = 1  },
                new OtherDerivedClass { SimpleField1 = 2  },
                new OtherDerivedClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<OtherDerivedClass>
            {
                new OtherDerivedClass { SimpleField1 = 1  },
                new OtherDerivedClass { SimpleField1 = 100 },
                new OtherDerivedClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { OtherClassCollectionField = nestedCollection2 };

            string errorMessage = string.Empty;
            string expectedMessage1 = "[1] =>";
            string expectedMessage2 = "OtherClassCollectionField";
            string expectedMessage3 = "SimpleField1";

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.OtherClassCollectionField, inChain)
                                        //.OnCompareFault(c => errorMessage = c.ShowFaults())
                                        //.OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2)
                                  && errorMessage.Contains(expectedMessage3);

            // Assert
            Assert.IsTrue(containMessage);
        }
        

        [TestMethod]
        [Ignore]
        public void SameClass_EqualBaseObjAssigned_ReusedComparer_ComparisonSuccess()
        {
            // Arrange
            var nestedCollection1 = new List<SimpleBaseClass>
            {
                new SimpleBaseClass { SimpleField1 = 1  },
                new SimpleBaseClass { SimpleField1 = 2  },
                new SimpleBaseClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<SimpleBaseClass>
            {
                new SimpleBaseClass { SimpleField1 = 1  },
                new SimpleBaseClass { SimpleField1 = 2  },
                new SimpleBaseClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { SameClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { SameClassCollectionField = nestedCollection2 };


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SameClassCollectionField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void SameClass_NotEqualBaseObjAssigned_ReusedComparer_ComparisonFail()
        {
            // Arrange
            var nestedCollection1 = new List<SimpleBaseClass>
            {
                new SimpleBaseClass { SimpleField1 = 1  },
                new SimpleBaseClass { SimpleField1 = 2  },
                new SimpleBaseClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<SimpleBaseClass>
            {
                new SimpleBaseClass { SimpleField1 = 1  },
                new SimpleBaseClass { SimpleField1 = 100  },
                new SimpleBaseClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { SameClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { SameClassCollectionField = nestedCollection2 };


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SameClassCollectionField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void SameClass_NotEqualBaseObjAssignedl_ReusedComparer_CorrectErrorMessageThrown()
        {
            // Arrange
            var nestedCollection1 = new List<SimpleBaseClass>
            {
                new SimpleBaseClass { SimpleField1 = 1  },
                new SimpleBaseClass { SimpleField1 = 2  },
                new SimpleBaseClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<SimpleBaseClass>
            {
                new SimpleBaseClass { SimpleField1 = 1  },
                new SimpleBaseClass { SimpleField1 = 100  },
                new SimpleBaseClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { SameClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { SameClassCollectionField = nestedCollection2 };

            string errorMessage = string.Empty;
            string expectedMessage1 = "[1] =>";
            string expectedMessage2 = "SameClassCollectionField";
            string expectedMessage3 = "SimpleField1";

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SameClassCollectionField)
                                        .Property(p => p.SimpleField1)
                                        //.OnCompareFault(c => errorMessage = c.ShowFaults())
                                        //.OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2)
                                  && errorMessage.Contains(expectedMessage3);

            // Assert
            Assert.IsTrue(containMessage);
        }

        [TestMethod]
        [Ignore]
        public void SameClass_EqualDerivedObjAssigned_ReusedComparer_ComparisonSuccess()
        {
            // Arrange
            var nestedCollection1 = new List<SimpleDerivedClass>
            {
                new SimpleDerivedClass { SimpleField1 = 1  },
                new SimpleDerivedClass { SimpleField1 = 2  },
                new SimpleDerivedClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<SimpleDerivedClass>
            {
                new SimpleDerivedClass { SimpleField1 = 1  },
                new SimpleDerivedClass { SimpleField1 = 2  },
                new SimpleDerivedClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { SameClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { SameClassCollectionField = nestedCollection2 };


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SameClassCollectionField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void SameClass_NotEqualDerivedObjAssigned_ReusedComparer_ComparisonFail()
        {
            // Arrange
            var nestedCollection1 = new List<SimpleDerivedClass>
            {
                new SimpleDerivedClass { SimpleField1 = 1  },
                new SimpleDerivedClass { SimpleField1 = 2  },
                new SimpleDerivedClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<SimpleDerivedClass>
            {
                new SimpleDerivedClass { SimpleField1 = 1  },
                new SimpleDerivedClass { SimpleField1 = 100  },
                new SimpleDerivedClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { SameClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { SameClassCollectionField = nestedCollection2 };


            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SameClassCollectionField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void SameClass_NotEqualDerivedObjAssigned_ReusedComparer_CorrectErrorMessageThrown()
        {
            // Arrange
            var nestedCollection1 = new List<SimpleDerivedClass>
            {
                new SimpleDerivedClass { SimpleField1 = 1  },
                new SimpleDerivedClass { SimpleField1 = 2  },
                new SimpleDerivedClass { SimpleField1 = 3  }
            };

            var nestedCollection2 = new List<SimpleDerivedClass>
            {
                new SimpleDerivedClass { SimpleField1 = 1  },
                new SimpleDerivedClass { SimpleField1 = 100  },
                new SimpleDerivedClass { SimpleField1 = 3  }
            };

            var a1 = new SimpleBaseClass { SameClassCollectionField = nestedCollection1 };
            var a2 = new SimpleBaseClass { SameClassCollectionField = nestedCollection2 };

            string errorMessage = string.Empty;
            string expectedMessage1 = "[1] =>";
            string expectedMessage2 = "SameClassCollectionField";
            string expectedMessage3 = "SimpleField1";

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .PropertyCollection(p => p.SameClassCollectionField)
                                        .Property(p => p.SimpleField1)
                                        //.OnCompareFault(c => errorMessage = c.ShowFaults())
                                        //.OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2)
                                  && errorMessage.Contains(expectedMessage3);

            // Assert
            Assert.IsTrue(containMessage);
        }

    }
}
