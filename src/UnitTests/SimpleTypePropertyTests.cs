﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation;
using UnitTests.TestClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Diagnostics;

namespace ComparisonChainFramework.UnitTests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class SimpleTypePropertyTests
    {
        [TestMethod]
        public void SimpleProperty_NoComparerSupplied_DefaultComparerFound()
        {
            // Arrange
            const int COMPARE_RESULT = 0;
            var mockCompareResult = new Mock<ICompareResult>();
            mockCompareResult.Setup(c => c.Result).Returns(COMPARE_RESULT);

            var a1 = new SimpleBaseClass { SimpleField1 = 1 };
            var a2 = new SimpleBaseClass { SimpleField1 = 2 };

            var customDefaultComparerProvider = new Mock<IComparerProvider>();
            var customIntComparer = new Mock<IComparerWrapper<int>>();
            customDefaultComparerProvider.Setup(dc => dc.GetComparer<int>(null)).Returns(customIntComparer.Object);
            customIntComparer.Setup(c => c.Compare(It.IsAny<int>(), It.IsAny<int>())).Returns(mockCompareResult.Object);

            // Act
            var chain = ComparerBuilder.Create<SimpleBaseClass>(customDefaultComparerProvider.Object)
                                       .Property(p => p.SimpleField1)
                                       .Build();

            var actual = chain.Compare(a1, a2);

            // Assert
            customDefaultComparerProvider.Verify(dc => dc.GetComparer<int>(null), Times.Once);
            customIntComparer.Verify(dc => dc.Compare(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void SimpleProperty_ComparerSupplied_SpecifiedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { SimpleField1 = 1 };
            var a2 = new SimpleBaseClass { SimpleField1 = 2 };

            var customComparer = new Mock<IComparer<int>>();

            customComparer.Setup(c => c.Compare(It.IsAny<int>(), It.IsAny<int>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .Property(p => p.SimpleField1, customComparer.Object)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void SingleStringProperty_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SingleStringClass { String1 = "str1" };
            var a2 = new SingleStringClass { String1 = "str1" };

            var cchain = ComparerBuilder.Create<SingleStringClass>().Property(p => p.String1).Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void DifferentPropertyValues_ComparisonFails()
        {
            // Arrange
            var a1 = new SingleStringClass { String1 = "str1" };
            var a2 = new SingleStringClass { String1 = "str2" };

            var cchain = ComparerBuilder.Create<SingleStringClass>().Property(p => p.String1).Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void DifferentPropertyValues_CorrectMessageThrown()
        {
            // Arrange
            var a1 = new SingleStringClass { String1 = "str1" };
            var a2 = new SingleStringClass { String1 = "str2" };

            string expectedMessage = "String1";
            string actualMessage = string.Empty;
            var cchain = ComparerBuilder.Create<SingleStringClass>()
                                        .Property(p => p.String1)
                                        .OnCompareFault(d => actualMessage = d.ShowFaults())
                                        .OnCompareFault(d => Debug.WriteLine(actualMessage))
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.IsTrue(actualMessage.Contains(expectedMessage));
        }

        [TestMethod]
        public void SeveralProperties_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SeveralStringClass { String1 = "str1", String2 = "str2", String3 = "str3" };
            var a2 = new SeveralStringClass { String1 = "str1", String2 = "str2", String3 = "str3" };

            var cchain =
                ComparerBuilder.Create<SeveralStringClass>()
                    .Property(p => p.String1)
                    .Property(p => p.String2)
                    .Property(p => p.String3)
                    .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void SeveralProperties_CorrectMessageThrown()
        {
            // Arrange
            var a1 = new SeveralStringClass { String1 = "str1", String2 = "str2", String3 = "str3" };
            var a2 = new SeveralStringClass { String1 = "str11", String2 = "str22", String3 = "str33" };

            string expectedMessage1 = "String1";
            string expectedMessage2 = "String2";
            string expectedMessage3 = "String3";

            string actualMessage = string.Empty;

            var cchain =
                ComparerBuilder.Create<SeveralStringClass>()
                    .Property(p => p.String1)
                    .Property(p => p.String2)
                    .Property(p => p.String3)
                    .OnCompareFault(d => actualMessage = d.ShowFaults())
                    .OnCompareFault(d => Debug.WriteLine(actualMessage))
                    .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.IsTrue(actualMessage.Contains(expectedMessage1) 
                          && actualMessage.Contains(expectedMessage2)
                          && actualMessage.Contains(expectedMessage3));
        }

        [TestMethod]
        public void OnePropertySkipped_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SeveralStringClass { String1 = "str1", String2 = "str2", String3 = "str3" };
            var a2 = new SeveralStringClass { String1 = "str1", String2 = "str2", String3 = "other string" };

            var cchain =
                ComparerBuilder.Create<SeveralStringClass>()
                    .Property(p => p.String1)
                    .Property(p => p.String2)
                    .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void OnePropertySkipped_CorrectMessageThrown()
        {
            // Arrange
            var a1 = new SeveralStringClass { String1 = "str1", String2 = "str2", String3 = "str3" };
            var a2 = new SeveralStringClass { String1 = "str11", String2 = "str2", String3 = "str33" };

            string expectedMessage1 = "String1";
            string expectedMessage2 = "String2";
            string expectedMessage3 = "String3";

            string actualMessage = string.Empty;

            var cchain =
                ComparerBuilder.Create<SeveralStringClass>()
                    .Property(p => p.String1)
                    .Property(p => p.String2)
                    .Property(p => p.String3)
                    .OnCompareFault(d => actualMessage = d.ShowFaults())
                    .OnCompareFault(d => Debug.WriteLine(actualMessage))
                    .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.IsTrue(actualMessage.Contains(expectedMessage1)
                          && actualMessage.Contains(expectedMessage3));
            Assert.IsFalse(actualMessage.Contains(expectedMessage2));
        }

        [TestMethod]
        public void Byte_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { Byte1 = 10 };
            var a2 = new SimpleTypesClass { Byte1 = 10 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.Byte1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void SByte_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { SByte2 = 11 };
            var a2 = new SimpleTypesClass { SByte2 = 11 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.SByte2)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Short_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { Short3 = 12 };
            var a2 = new SimpleTypesClass { Short3 = 12 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.Short3)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void UShort_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { UShort4 = 13 };
            var a2 = new SimpleTypesClass { UShort4 = 13 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.UShort4)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Int_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { Int5 = 14 };
            var a2 = new SimpleTypesClass { Int5 = 14 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.Int5)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void UInt_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { UInt6 = 15 };
            var a2 = new SimpleTypesClass { UInt6 = 15 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.UInt6)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Long_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { Long7 = 16 };
            var a2 = new SimpleTypesClass { Long7 = 16 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.Long7)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void ULong_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { ULong8 = 17 };
            var a2 = new SimpleTypesClass { ULong8 = 17 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.ULong8)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Float_CorrectlyCompares()
        {
            // TODO: Add cases for comparing with delta
            // Arrange
            var a1 = new SimpleTypesClass { Float9 = 18f };
            var a2 = new SimpleTypesClass { Float9 = 18f };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.Float9)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Double_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { Double10 = 19 };
            var a2 = new SimpleTypesClass { Double10 = 19 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.Double10)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Decimal_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { Decimal11 = 20 };
            var a2 = new SimpleTypesClass { Decimal11 = 20 };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.Decimal11)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Bool_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { Bool12 = true };
            var a2 = new SimpleTypesClass { Bool12 = true };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.Bool12)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void Char_CorrectlyCompares()
        {
            // Arrange
            var a1 = new SimpleTypesClass { Char13 = 'c' };
            var a2 = new SimpleTypesClass { Char13 = 'c' };

            var cchain = ComparerBuilder.Create<SimpleTypesClass>()
                                        .Property(p => p.Char13)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }
    }
}
