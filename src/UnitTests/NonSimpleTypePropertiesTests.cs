﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using UnitTests.TestClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ComparisonChain.Implementation;

namespace ComparisonChainFramework.UnitTests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class NonSimpleTypePropertiesTests
    {
        private const string FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE = "First parameter is Null!";

        private const string SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE = "Second parameter is Null!";


        #region Basic checks

        [TestMethod]
        [Ignore]
        [ExpectedException(typeof(InvalidOperationException), AllowDerivedTypes = false)]
        public void NoComparerSupplied_InvalidOperationExceptionThrown()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 5 } };

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .Property(p => p.OtherBaseClassField)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
        }

        [TestMethod]
        [Ignore]
        public void PropertyOfFirstIsNull_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass();
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } };

            var nestedComparer = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .SharedComparer<OtherBaseClass>(nestedComparer)
                                        .Property(p => p.OtherBaseClassField)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void PropertyOfSecondIsNull_ComparisonFail()
        {
            // Arrange          
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass();

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .SharedComparer<OtherBaseClass>(inChain)
                                        .Property(p => p.OtherBaseClassField)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void PropertyOfFirstIsNull_CorrectErrorMessageThrown()
        {
            // Arrange
            var a1 = new SimpleBaseClass();
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } };

            string errorMessage = string.Empty;
            string expectedMessage1 = "OtherBaseClassField";
            string expectedMessage2 = FIRST_PARAMETER_IS_NULL_ERROR_MESSAGE;

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .SharedComparer<OtherBaseClass>(inChain)
                                        .Property(p => p.OtherBaseClassField)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2);


            // Assert
            Assert.IsTrue(containMessage);
        }

        [TestMethod]
        [Ignore]
        public void PropertyOfSecondIsNull_CorrectErrorMessageThrown()
        {
            // Arrange          
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass();

            string errorMessage = string.Empty;
            string expectedMessage1 = "OtherBaseClassField";
            string expectedMessage2 = SECOND_PARAMETER_IS_NULL_ERROR_MESSAGE;

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .SharedComparer<OtherBaseClass>(inChain)
                                        .Property(p => p.OtherBaseClassField)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .OnCompareFault(c => Debug.WriteLine(c.ShowFaults()))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2);

            // Assert
            Assert.IsTrue(containMessage);
        }

        [TestMethod]
        [Ignore]
        public void PropertyOfBothIsNull_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherBaseClassField = null };
            var a2 = new SimpleBaseClass { OtherBaseClassField = null};

            var nestedComparer = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherBaseClassField, nestedComparer)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        #endregion


        #region Correct comparer invoke tests

        [TestMethod]
        [Ignore]
        public void BaseClassProperty_BaseClassComparerSupplied_SuppliedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<OtherBaseClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<OtherBaseClass>(), It.IsAny<OtherBaseClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .Property(p => p.OtherBaseClassField, customComparer.Object)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<OtherBaseClass>(), It.IsAny<OtherBaseClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void BaseClassProperty_BaseClassComparerNested_NestedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<OtherBaseClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<OtherBaseClass>(), It.IsAny<OtherBaseClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .SharedComparer(customComparer.Object)
                                       .Property(p => p.OtherBaseClassField)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<OtherBaseClass>(), It.IsAny<OtherBaseClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedClassProperty_BaseClassComparerNested_NestedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<OtherBaseClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<OtherBaseClass>(), It.IsAny<OtherBaseClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .SharedComparer(customComparer.Object)
                                       .Property(p => p.OtherDerivedClassField)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<OtherBaseClass>(), It.IsAny<OtherBaseClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedClassProperty_BaseClassComparerSupplied_SuppliedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<OtherBaseClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<OtherBaseClass>(), It.IsAny<OtherBaseClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .Property(p => p.OtherDerivedClassField, customComparer.Object)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<OtherBaseClass>(), It.IsAny<OtherBaseClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedClassProperty_DerivedClassComparerNested_NestedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<OtherDerivedClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<OtherDerivedClass>(), It.IsAny<OtherDerivedClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .SharedComparer(customComparer.Object)
                                       .Property(p => p.OtherDerivedClassField)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<OtherDerivedClass>(), It.IsAny<OtherDerivedClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedClassProperty_DerivedClassComparerSupplied_SuppliedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<OtherDerivedClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<OtherDerivedClass>(), It.IsAny<OtherDerivedClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .Property(p => p.OtherDerivedClassField, customComparer.Object)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<OtherDerivedClass>(), It.IsAny<OtherDerivedClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void SameClassProperty_BaseClassComparerSupplied_SuppliedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { SameClassField = new SimpleBaseClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { SameClassField = new SimpleBaseClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<SimpleBaseClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<SimpleBaseClass>(), It.IsAny<SimpleBaseClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .Property(p => p.SameClassField, customComparer.Object)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<SimpleBaseClass>(), It.IsAny<SimpleBaseClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void SameClassProperty_BaseClassComparerNested_NestedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { SameClassField = new SimpleBaseClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { SameClassField = new SimpleBaseClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<SimpleBaseClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<SimpleBaseClass>(), It.IsAny<SimpleBaseClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .SharedComparer(customComparer.Object)
                                       .Property(p => p.SameClassField)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<SimpleBaseClass>(), It.IsAny<SimpleBaseClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedFromSameClassProperty_BaseClassComparerSupplied_SuppliedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<SimpleBaseClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<SimpleBaseClass>(), It.IsAny<SimpleBaseClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .Property(p => p.DerivedFromSameClassField, customComparer.Object)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<SimpleBaseClass>(), It.IsAny<SimpleBaseClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedFromSameClassProperty_BaseClassComparerNested_NestedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<SimpleBaseClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<SimpleBaseClass>(), It.IsAny<SimpleBaseClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .SharedComparer(customComparer.Object)
                                       .Property(p => p.DerivedFromSameClassField)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<SimpleBaseClass>(), It.IsAny<SimpleBaseClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedFromSameClassProperty_DerivedClassComparerSupplied_SuppliedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<SimpleDerivedClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<SimpleDerivedClass>(), It.IsAny<SimpleDerivedClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .Property(p => p.DerivedFromSameClassField, customComparer.Object)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<SimpleDerivedClass>(), It.IsAny<SimpleDerivedClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedFromSameClassProperty_DerivedClassComparerNested_NestedComparerInvoked()
        {
            // Arrange
            const int COMPARE_RESULT = 0;

            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 2 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 5 } };

            var customComparer = new Mock<IComparer<SimpleDerivedClass>>();

            customComparer.Setup(c => c.Compare(It.IsAny<SimpleDerivedClass>(), It.IsAny<SimpleDerivedClass>())).Returns(COMPARE_RESULT);

            var chain = ComparerBuilder.Create<SimpleBaseClass>()
                                       .SharedComparer(customComparer.Object)
                                       .Property(p => p.DerivedFromSameClassField)
                                       .Build();

            // Act
            var actual = chain.Compare(a1, a2);

            // Assert
            customComparer.Verify(c => c.Compare(It.IsAny<SimpleDerivedClass>(), It.IsAny<SimpleDerivedClass>()), Times.Once);
            Assert.AreEqual(0, actual);
        }

        #endregion


        #region Other class property


        [TestMethod]
        [Ignore]
        public void BaseClass_EqualBaseObjAssigned_BaseClassComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } };


            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherBaseClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void BaseClassNotEqualBaseObjAssigned_BaseClassComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 100 } };


            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherBaseClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }
        
        [TestMethod]
        [Ignore]
        public void BaseClass_EqualDerivedObjAssigned_BaseClassComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherDerivedClass { SimpleField1 = 1 } };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherBaseClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void BaseClass_NotEqualDerivedObjAssigned_BaseClassComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherDerivedClass { SimpleField1 = 100 } };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherBaseClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void BaseClass_EqualDerivedObjAssigned_DerivedClassComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherDerivedClass { SimpleField1 = 1 } };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherBaseClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void BaseClass_NotEqualDerivedObjAssigned_DerivedClassComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherDerivedClass { SimpleField1 = 100 } };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherBaseClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }


        [TestMethod]
        [Ignore]
        public void DerivedClass_EqualObjects_BaseClassComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 1 } };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherDerivedClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedClass_NotEqualObjects_BaseClassComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 100 } };

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherDerivedClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedClass_EqualObjects_DerivedClassComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 1 } };

            var inChain = ComparerBuilder.Create<OtherDerivedClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherDerivedClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedClass_NotEqualObjects_DerivedClassComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherDerivedClassField = new OtherDerivedClass { SimpleField1 = 100 } };

            var inChain = ComparerBuilder.Create<OtherDerivedClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherDerivedClassField, inChain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        public void NonSimplePropertiesNotEqual_CorrectErrorMessageThrown()
        {
            // Arrange
            var a1 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { OtherBaseClassField = new OtherBaseClass { SimpleField1 = 100 } };

            string expectedMessage1 = "OtherBaseClassField";
            string expectedMessage2 = "SimpleField1";
            string errorMessage = string.Empty;

            var inChain = ComparerBuilder.Create<OtherBaseClass>()
                                         .Property(n => n.SimpleField1)
                                         .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.OtherBaseClassField, inChain)
                                        .OnCompareFault(c => errorMessage = c.ShowFaults())
                                        .OnCompareFault(c => Debug.WriteLine(errorMessage))
                                        .Build();

            // Act
            cchain.Compare(a1, a2);
            bool containMessage = errorMessage.Contains(expectedMessage1)
                                  && errorMessage.Contains(expectedMessage2);


            // Assert
            Assert.IsTrue(containMessage);
        }

        #endregion


        #region Same class property

        [TestMethod]
        [Ignore]
        public void SameClass_EqualBaseObjAssigned_ReusedComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SameClassField = new SimpleBaseClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { SameClassField = new SimpleBaseClass { SimpleField1 = 1 } };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SameClassField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void SameClass_NotEqualBaseObjAssigned_ReusedComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SameClassField = new SimpleBaseClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { SameClassField = new SimpleBaseClass { SimpleField1 = 100 } };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SameClassField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void SameClass_EqualDerivedObjAssigned_ReusedComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { SameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SameClassField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void SameClass_NotEqualDerivedObjAssigned_ReusedComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { SameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { SameClassField = new SimpleDerivedClass { SimpleField1 = 100 } };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SameClassField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }
        
        
        [TestMethod]
        [Ignore]
        public void DerivedFromSameClass_EqualObj_ReusedComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.DerivedFromSameClassField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedFromSameClass_NotEqualObj_ReusedComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 100 } };

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.DerivedFromSameClassField)
                                        .Property(p => p.SimpleField1)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedFromSameClass_EqualObj_BaseClassComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };

            var inchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.DerivedFromSameClassField, inchain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedFromSameClass_NotEqualObj_BaseClassComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 100 } };

            var inchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.DerivedFromSameClassField, inchain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedFromSameClass_EqualObjects_DerivedClassComparer_ComparisonSuccess()
        {
            // Arrange
            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };

            var inchain = ComparerBuilder.Create<SimpleDerivedClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.DerivedFromSameClassField, inchain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        [Ignore]
        public void DerivedFromSameClass_NotEqualObj_DerivedClassComparer_ComparisonFail()
        {
            // Arrange
            var a1 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 1 } };
            var a2 = new SimpleBaseClass { DerivedFromSameClassField = new SimpleDerivedClass { SimpleField1 = 100 } };

            var inchain = ComparerBuilder.Create<SimpleDerivedClass>()
                                        .Property(p => p.SimpleField1)
                                        .Build();

            var cchain = ComparerBuilder.Create<SimpleBaseClass>()
                                        .Property(p => p.DerivedFromSameClassField, inchain)
                                        .Build();

            // Act
            var actual = cchain.Compare(a1, a2);

            // Assert
            Assert.AreNotEqual(0, actual);
        }

        #endregion

    }
}
