﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ComparisonChain.Contracts;

namespace ComparisonChain.Implementation.FunctorWrappers
{
    /// <summary>
    /// Provides the way to get IEnumerable of TResult value from the specified TResult[] property in T objects 
    /// </summary>
    /// <typeparam name="T">Type of object which property value will be returned</typeparam>
    /// <typeparam name="TResult">Type of items in the array in specified property</typeparam>
    internal class ArrayFunctorWrapper<T,TResult> : FunctorWrapper, IFunctorWrapper<T, IEnumerable<TResult>>
    {
        private readonly Expression<Func<T, TResult[]>> _functor;
        private Func<T, TResult[]> _compiledFunctor;
        private Func<T, TResult[]> PathFunc
        {
            get{
                if(_compiledFunctor == null)
                {
                    _compiledFunctor = _functor.Compile();
                }
                return _compiledFunctor;
            }
        }

        /// <summary>
        /// Gets expression which provide the way to compared property 
        /// </summary>
        protected override LambdaExpression Functor
        {
            get
            {
                return _functor;
            }
        }

        /// <summary>
        /// Creates an instance of ArrayFunctorWrapper class
        /// </summary>
        /// <param name="functor">Path to the property</param>
        internal ArrayFunctorWrapper(Expression<Func<T, TResult[]>> functor)
        {
            if (functor == null)
            {
                throw new ArgumentNullException("ArrayFunctorWrapper constructor");
            }
            this._functor = functor;
        }

        /// <summary>
        /// Returns value of the specified property from the source object 
        /// </summary>
        /// <param name="sourceObject">Object which property value is requested</param>
        /// <returns>Property value</returns>
        public IEnumerable<TResult> GetPropertyValue(T sourceObject)
        {
            return PathFunc(sourceObject).AsEnumerable();
        }
    }
}
