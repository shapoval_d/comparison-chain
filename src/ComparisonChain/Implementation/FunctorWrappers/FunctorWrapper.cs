﻿using System.Linq.Expressions;

namespace ComparisonChain.Implementation.FunctorWrappers
{
    /// <summary>
    /// Base class for functor wrappers
    /// </summary>
    internal abstract class FunctorWrapper
    {
        private string _propertyPath = null;

        /// <summary>
        /// Gets expression which provide the way to compared property 
        /// </summary>
        protected abstract LambdaExpression Functor { get;}

        /// <summary>
        /// Returns path to compared property
        /// </summary>
        /// <returns>Path to compared property</returns>
        public string GetPropertyPath()
        {
            if (_propertyPath == null)
            {
                _propertyPath = Functor.Body.ToString();
            }

            return _propertyPath;
        }
    }
}
