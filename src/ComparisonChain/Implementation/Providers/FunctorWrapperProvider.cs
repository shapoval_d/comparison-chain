﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation.FunctorWrappers;

namespace ComparisonChain.Implementation.Providers
{
    /// <summary>
    /// Provides the way to get correct functor wrapper according to the type of specified property
    /// </summary>
    internal static class FunctorWrapperProvider
    {
        /// <summary>
        /// Returns functor wrapper for property with TResult type
        /// </summary>
        /// <typeparam name="T">Type of object which contain specified TResult property</typeparam>
        /// <typeparam name="TResult">Type of property in the T object</typeparam>
        /// <param name="functor">Path to the property in T object</param>
        /// <returns>Functor wrapper for property with TResult type</returns>
        internal static IFunctorWrapper<T, TResult> GetWrapper<T, TResult>(Expression<Func<T, TResult>> functor)
        {
            return new SingleItemFunctorWrapper<T, TResult>(functor);
        }

        /// <summary>
        /// Returns functor wrapper for property with TResult[] type
        /// </summary>
        /// <typeparam name="T">Type of object which contain specified TResult[] property</typeparam>
        /// <typeparam name="TResult">Type of items in array-based property in the T object</typeparam>
        /// <param name="functor">Path to the property in T object</param>
        /// <returns></returns>
        internal static IFunctorWrapper<T, IEnumerable<TResult>> GetWrapper<T, TResult>(Expression<Func<T, TResult[]>> functor)
        {
            return new ArrayFunctorWrapper<T, TResult>(functor);
        }

        /// <summary>
        /// Returns functor wrapper for property with IEnumerable of TResult type
        /// </summary>
        /// <typeparam name="T">Type of object which contain specified IEnumerable of TResult property</typeparam>
        /// <typeparam name="TResult">Type of items in collection-based property in the T object</typeparam>
        /// <param name="functor">Path to the property in T object</param>
        /// <returns></returns>
        internal static IFunctorWrapper<T, IEnumerable<TResult>> GetWrapper<T, TResult>(Expression<Func<T, IEnumerable<TResult>>> functor)
        {
            return new CollectionFunctorWrapper<T,TResult>(functor);
        }
    }
}
