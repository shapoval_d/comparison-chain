﻿using System;
using System.Collections.Generic;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation.ComparerWrappers;

namespace ComparisonChain.Implementation.Providers
{
    /// <summary>
    /// Provides the way to get on demand default or registered comparers
    /// </summary>
    internal class ComparerProvider : IComparerProvider
    {
        private readonly IDictionary<Type, object> _comparers = new Dictionary<Type, object>();

        /// <summary>
        /// Registers comparer for TResult object
        /// </summary>
        /// <typeparam name="TResult">Type of objects which will be compared by specified comparer</typeparam>
        /// <param name="comparer">Comparer which will be used to compare TResult objects</param>
        public void RegisterComparer<TResult>(IComparer<TResult> comparer)
        {
            var currentType = typeof(TResult);
            if (_comparers.ContainsKey(currentType))
            {
                var message = string.Format("Comparer for type {0} is already registered", currentType); 
                throw new ArgumentException(message);
            }

            _comparers.Add(currentType, comparer);
        }

        /// <summary>
        /// Provide comparer wrapper, which allow to compare TResult objects
        /// </summary>
        /// <typeparam name="TResult">Type of objects which will be compared by objects</typeparam>
        /// <param name="comparer">Comparer of TResult objects</param>
        /// <returns></returns>
        public IComparerWrapper<TResult> GetComparer<TResult>(IComparer<TResult> comparer)
        {
            IComparerWrapper<TResult> result = null;
            if (comparer == null)
            {
                result = GetDefaultComparer<TResult>();
            }
            else
            {
                result = new SingleItemComparerWrapper<TResult>(comparer);
            }

            return result;
        }

        /// <summary>
        /// Provide comparer wrapper, which allow to compare collections of TResult objects
        /// </summary>
        /// <typeparam name="TResult">Type of collection items which will be compared by objects</typeparam>
        /// <param name="comparer">Comparer of TResult objects</param>
        /// <returns></returns>
        public IComparerWrapper<IEnumerable<TResult>> GetCollectionComparer<TResult>(IComparer<TResult> comparer)
        {
            IComparerWrapper<IEnumerable<TResult>> result = null;
            IComparerWrapper<TResult> itemComparer = null;

            itemComparer = (comparer == null)
                            ? GetDefaultComparer<TResult>()
                            : new SingleItemComparerWrapper<TResult>(comparer);

            if (itemComparer != null)
            {
                result = new EnumerableComparerWrapper<TResult>(itemComparer);
            }

            return result;
        }

        /// <summary>
        /// Provides comparer wrapper in compare-runtime
        /// </summary>
        /// <typeparam name="T">Type of objects which will be compared by objects</typeparam>
        /// <returns>Comparer of TResult objects</returns>
        public IComparerWrapper<T> GetRuntimeComparer<T>()
        {
            throw new NotImplementedException("ComparerProvider.GetRuntimeComparer");
        }

        private IComparerWrapper<TResult> GetDefaultComparer<TResult>()
        {
            var currentType = typeof(TResult);
            IComparerWrapper<TResult> result = null;

            if (IsNullCheckable(currentType))
            {
                result = new NullCheckComparerWrapper<TResult>(Comparer<TResult>.Default);
            }
            else if (TypeIsPrimitive(currentType))
            {
                result = new SingleItemComparerWrapper<TResult>(Comparer<TResult>.Default);
            }

            return result;
        }

        private static bool TypeIsPrimitive(Type type)
        {
            var typeCode = Type.GetTypeCode(type);
            return typeCode != TypeCode.Empty && typeCode != TypeCode.Object;
        }

        private static bool IsNullable(Type type)
        {
            var underType = Nullable.GetUnderlyingType(type);
            return underType != null && TypeIsPrimitive(underType);
        }

        private static bool IsNullCheckable(Type type)
        {
            return IsNullable(type) || Type.GetTypeCode(type) == TypeCode.String;
        }
    }
}
