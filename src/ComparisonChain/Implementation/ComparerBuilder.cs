﻿using ComparisonChain.Contracts;

namespace ComparisonChain.Implementation
{
    /// <summary>
    /// Provides the builder of customized comparers
    /// </summary>
    public static class ComparerBuilder
    {
        /// <summary>
        /// Returns builder for comparison chain which will allow to compare T objects
        /// </summary>
        /// <typeparam name="T">Type of object will be compared by constructed comparison chain</typeparam>
        /// <returns>Builder for comparison chain</returns>
        public static ComparisonChainBuilder<T> Create<T>()
        {
            return new ComparisonChainBuilder<T>();
        }

        /// <summary>
        /// Returns builder for comparison chain which will allow to compare T objects
        /// </summary>
        /// <typeparam name="T">Type of object will be compared by constructed comparison chain</typeparam>
        /// <param name="defaultComparerProvider">Comparer provider will be used to find default comparers in cases when comparer will not specified</param>
        /// <returns>Builder for comparison chain</returns>
        public static ComparisonChainBuilder<T> Create<T>(IComparerProvider defaultComparerProvider)
        {
            return new ComparisonChainBuilder<T>(defaultComparerProvider);
        }
    }
}
