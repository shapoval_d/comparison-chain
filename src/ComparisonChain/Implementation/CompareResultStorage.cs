﻿using ComparisonChain.Contracts;
using System.Collections.Generic;
using System.Text;

namespace ComparisonChain.Implementation
{
    /// <summary>
    /// Represents integrated result of compare of two objects
    /// </summary>
    public class CompareResultStorage : ICompareResultStorage
    {
        private readonly List<ICompareResult> _faultyResults = new List<ICompareResult>();
        private int _result;

        /// <summary>
        /// Gets collection of faulted compare results
        /// </summary>    
        public IList<ICompareResult> FaultyResults
        {
            get { return this._faultyResults; }
        }

        /// <summary>
        /// Gets or sets number representation of comparison result of property in two compared objects
        /// A signed integer that indicates the relative values of two compared objects. Value Meaning Less than zero first of compared is less than second. Zero compared objects are equals. Greater than zero first of compare is greater than second.
        /// </summary>
        public int Result
        {
            get { return _result; }
        }

        /// <summary>
        /// Processes internal comparison result  
        /// </summary>
        /// <param name="internalResult">Result of compare</param>
        public void AddCompareResult(ICompareResult internalResult)
        {
            if (internalResult.Result != 0)
            {
                this._result = internalResult.Result;
                _faultyResults.Add(internalResult);
            }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        public string ShowFaults()
        {
            var result = new StringBuilder();
            foreach (var item in _faultyResults)
            {
                result.AppendLine(item.ShowFaults());
            }

            return result.ToString();
        }
    }
}
