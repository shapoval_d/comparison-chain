﻿using System;
using System.Collections.Generic;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation.ComparisonLinks;
using ComparisonChain.Implementation.Providers;
using ComparisonChain.Implementation.CompareResults;

namespace ComparisonChain.Implementation
{
    /// <summary>
    /// Provides the way to custom compare to T objects 
    /// </summary>
    /// <typeparam name="T">Type of compared objects</typeparam>
    public class ComparisonChain<T> : IComparisonChain<T>
    {
        private readonly bool _needNullCheck;

        /// <summary>
        /// Event which fired when compare faults
        /// </summary>
        internal event Action<CompareResultStorage> OnCompareFault;

        /// <summary>
        /// Object which provides default and registered comparers
        /// </summary>
        internal IComparerProvider ComparerProvider { get; set; }

        /// <summary>
        /// Links are used to compare properties in T objects
        /// </summary>
        internal List<IComparisonLink<T>> ComparisonLinks { get; private set; }      

        /// <summary>
        /// Creates an instance of ComparisonChain class
        /// </summary>
        internal ComparisonChain()
        {
            this.ComparisonLinks = new List<IComparisonLink<T>>();
            this.ComparerProvider = new ComparerProvider();

            _needNullCheck = NeedNullCheck();
        }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x"/> and <paramref name="y"/>, as shown in the following table.Value Meaning Less than zero<paramref name="x"/> is less than <paramref name="y"/>.Zero<paramref name="x"/> equals <paramref name="y"/>.Greater than zero<paramref name="x"/> is greater than <paramref name="y"/>.
        /// </returns>
        /// <param name="x">The first object to compare.</param><param name="y">The second object to compare.</param>
        public int Compare(T x, T y)
        {
            var result = InternalCompare(x, y);

            if (OnCompareFault != null)
            {
                OnCompareFault(result);
            }

            return result.Result;
        }

        /// <summary>
        /// Compares two T objects and returns a value indicating whether one is less than, equal to, or greater than the other. 
        /// </summary>
        /// <param name="x">First of compared</param>
        /// <param name="y">Second of compared</param>
        /// <returns>Compare result storage</returns>
        internal CompareResultStorage InternalCompare(T x, T y)
        {
            var compareResultStorage = new CompareResultStorage();
            if (_needNullCheck)
            {

                if (x == null && y != null)
                {
                    var compareResult = new SimpleCompareResult
                    {
                        Result = -1,
                        Message = ComparisonChainConstants.FIRST_IS_NULL
                    };
                    compareResultStorage.AddCompareResult(compareResult);
                }
                else if (x != null && y == null)
                {
                    var compareResult = new SimpleCompareResult
                    {
                        Result = 1,
                        Message = ComparisonChainConstants.SECOND_IS_NULL
                    };
                    compareResultStorage.AddCompareResult(compareResult);
                }
                else if (x == null && y == null)
                {
                    var compareResult = new SimpleCompareResult { Result = 0 };
                    compareResultStorage.AddCompareResult(compareResult);
                }
                else
                {
                    CompareWithLinks(x, y, compareResultStorage);
                }
            }
            else
            {
                CompareWithLinks(x, y, compareResultStorage);
            }

            return compareResultStorage;    
        }

        private void CompareWithLinks(T first, T second, CompareResultStorage compareResultStorage)
        {
            foreach (var link in this.ComparisonLinks)
            {
                var internalCompareResult = link.Compare(first, second);
                compareResultStorage.AddCompareResult(internalCompareResult);
            }
        }

        private static bool NeedNullCheck()
        {
            var result = false;
            var currentType = typeof(T);
            if (currentType.IsClass || TypeIsNullable(currentType))
            {
                result = true;
            }

            return result;
        }

        private static bool TypeIsNullable(Type currentType)
        {
            return Nullable.GetUnderlyingType(currentType) != null;
        }
    }
}
