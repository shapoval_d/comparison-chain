﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation.ComparisonLinks;
using ComparisonChain.Implementation.Providers;

namespace ComparisonChain.Implementation
{
    /// <summary>
    /// Provides the way to create comparison chain will be used to compare T objects
    /// </summary>
    /// <typeparam name="T">Type of object will be compared by constructed comparison chain</typeparam>
    public class ComparisonChainBuilder<T>
    {
        private readonly ComparisonChain<T> _comparisonChain;

        /// <summary>
        /// Creates an instance of ComparisonChainBuilder class
        /// </summary>
        public ComparisonChainBuilder()
        {
            this._comparisonChain = new ComparisonChain<T>();
        }

        /// <summary>
        /// Creates an instance of ComparisonChainBuilder class
        /// </summary>
        /// <param name="defaultComparerProvider">>Comparer provider will be used to find default comparers in cases when comparer will not specified</param>
        public ComparisonChainBuilder(IComparerProvider defaultComparerProvider)
            :this()
        {
            _comparisonChain.ComparerProvider = defaultComparerProvider;
        }

        /// <summary>
        /// Registers that property specified by path function should be checked during comparing T oblects
        /// </summary>
        /// <typeparam name="TResult">Type of the specified property in T object</typeparam>
        /// <param name="path">Express function which describes the way to the specified property</param>
        /// <returns>Current comparison chain builder</returns>
        public ComparisonChainBuilder<T> Property<TResult>(Expression<Func<T, TResult>> path)
        {
            var link = CreateLink<TResult>(path, null);
            AddLinkToChain(link);
            return this;
        }

        /// <summary>
        /// Registers that property specified by path should be checked during comparing T oblects
        /// </summary>
        /// <typeparam name="TResult">Type of the specified property in T object</typeparam>
        /// <param name="path">Express function which describes the way to the specified property</param>
        /// <param name="comparer">Comparer which will be used to compare values in the specified property</param>
        /// <returns>Current comparison chain builder</returns>
        public ComparisonChainBuilder<T> Property<TResult>(Expression<Func<T, TResult>> path, IComparer<TResult> comparer)
        {
            var link = CreateLink<TResult>(path, comparer);
            AddLinkToChain(link);
            return this;
        }

        /// <summary>
        /// Registers that property specified by path should be checked during comparing T oblects
        /// </summary>
        /// <typeparam name="TResult">Type of the items in array in the specified property in T object</typeparam>
        /// <param name="path">Express function which describes the way to the specified property</param>
        /// <returns>Current comparison chain builder</returns>
        public ComparisonChainBuilder<T> PropertyCollection<TResult>(Expression<Func<T, TResult[]>> path)
        {
            var link = CreateLink<TResult>(path, null);
            AddLinkToChain(link);
            return this;
        }

        /// <summary>
        /// Registers that property specified by path should be checked during comparing T oblects
        /// </summary>
        /// <typeparam name="TResult">Type of the items in array in the specified property in T object</typeparam>
        /// <param name="path">Express function which describes the way to the specified property</param>
        /// <param name="comparer">Comparer which will be used to compare items in arrays in the specified property</param>
        /// <returns>Current comparison chain builder</returns>
        public ComparisonChainBuilder<T> PropertyCollection<TResult>(Expression<Func<T, TResult[]>> path, IComparer<TResult> comparer)
        {
            var link = CreateLink<TResult>(path, comparer);
            AddLinkToChain(link);
            return this;
        }
        
        /// <summary>
        /// Registers that property specified by path should be checked during comparing T oblects
        /// </summary>
        /// <typeparam name="TResult">Type of the items in collecton in the specified property in T object</typeparam>
        /// <param name="path">Express function which describes the way to the specified property</param>
        /// <returns>Current comparison chain builder</returns>
        public ComparisonChainBuilder<T> PropertyCollection<TResult>(Expression<Func<T, IEnumerable<TResult>>> path)
        {
            var link = CreateLink<TResult>(path, null);
            AddLinkToChain(link);
            return this;
        }

        /// <summary>
        /// Registers that property specified by path should be checked during comparing T oblects
        /// </summary>
        /// <typeparam name="TResult">Type of the items in collecton in the specified property in T object</typeparam>
        /// <param name="path">Express function which describes the way to the specified property</param>
        /// <param name="comparer">Comparer which will be used to compare items in collection in the specified property</param>
        /// <returns>Current comparison chain builder</returns>
        public ComparisonChainBuilder<T> PropertyCollection<TResult>(Expression<Func<T, IEnumerable<TResult>>> path, IComparer<TResult> comparer)
        {
            var link = CreateLink<TResult>(path, comparer);
            AddLinkToChain(link);
            return this;
        }

        /// <summary>
        /// Registers comparer will be used to compare TResult objects
        /// </summary>
        /// <typeparam name="TResult">Type of objects will be compared by specified comparer</typeparam>
        /// <param name="comparer">Comparer which will be used to compare TResult objects</param>
        /// <returns>Current comparison chain builder</returns>
        public ComparisonChainBuilder<T> SharedComparer<TResult>(IComparer<TResult> comparer)
        {
            _comparisonChain.ComparerProvider.RegisterComparer<TResult>(comparer);
            return this;
        }

        /// <summary>
        /// Registers handler to event when compare faults
        /// </summary>
        /// <param name="action">Handler for event</param>
        /// <returns>Current comparison chain builder</returns>
        public ComparisonChainBuilder<T> OnCompareFault(Action<CompareResultStorage> action)
        {
            this._comparisonChain.OnCompareFault += action;
            return this;
        }

        /// <summary>
        /// Returns constructed comparison chain
        /// </summary>
        /// <returns>Constructed comparison chain</returns>
        public ComparisonChain<T> Build()
        {
            return this._comparisonChain;
        }

        #region Private

        private void AddLinkToChain(IComparisonLink<T> link)
        {
            this._comparisonChain.ComparisonLinks.Add(link);
        }

        private IComparisonLink<T> CreateLink<TResult>(Expression<Func<T, TResult>> pathFunc, IComparer<TResult> comparer)
        {
            var functorWrapper = FunctorWrapperProvider.GetWrapper<T, TResult>(pathFunc);
            var compareWrapper = this._comparisonChain.ComparerProvider.GetComparer<TResult>(comparer);  
            return new SingleItemComparisonLink<T, TResult>(functorWrapper, compareWrapper);
        }

        private IComparisonLink<T> CreateLink<TResult>(Expression<Func<T, IEnumerable<TResult>>> pathFunc, IComparer<TResult> comparer)
        {
            var functorWrapper = FunctorWrapperProvider.GetWrapper<T, TResult>(pathFunc);
            var compareWrapper = this._comparisonChain.ComparerProvider.GetCollectionComparer<TResult>(comparer);
            return new CollectionComparisonLink<T, TResult>(functorWrapper, compareWrapper);
        }

        private IComparisonLink<T> CreateLink<TResult>(Expression<Func<T, TResult[]>> pathFunc,IComparer<TResult> comparer)
        {
            var functorWrapper = FunctorWrapperProvider.GetWrapper<T, TResult>(pathFunc);
            var compareWrapper = this._comparisonChain.ComparerProvider.GetCollectionComparer<TResult>(comparer);        
            return new CollectionComparisonLink<T, TResult>(functorWrapper, compareWrapper);
        }

        #endregion
    }
}
