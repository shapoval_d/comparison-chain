﻿using System.Collections.Generic;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation.CompareResults;

namespace ComparisonChain.Implementation.ComparerWrappers
{
    /// <summary>
    /// Provides the way to compare TResult objects (include null comparing)
    /// </summary>
    /// <typeparam name="TResult">Type of compared objects</typeparam>
    internal class NullCheckComparerWrapper<TResult> : IComparerWrapper<TResult>
    {
        private readonly SingleItemComparerWrapper<TResult> _innerComparer;

        /// <summary>
        /// Creates an instance of NullCheckSingleItemComparer class
        /// </summary>
        /// <param name="comparer">Comparer, which will be used to compare TResult objects</param>
        internal NullCheckComparerWrapper(IComparer<TResult> comparer)
        {
            this._innerComparer = new SingleItemComparerWrapper<TResult>(comparer);
        }

        /// <summary>
        /// Returns result of compare of two TResult objects
        /// </summary>
        /// <param name="x">First of compared</param>
        /// <param name="y">Second of compared</param>
        /// <returns>Result of compare of two TResult objects</returns>
        public ICompareResult Compare(TResult x, TResult y)
        {
            ICompareResult result = null;
            if (x == null && y != null)
            {
                result = new SimpleCompareResult
                {
                    Result = -1,
                    Message = ComparisonChainConstants.FIRST_IS_NULL
                };
            }
            else if (x != null && y == null)
            {
                result = new SimpleCompareResult
                {
                    Result = 1,
                    Message = ComparisonChainConstants.SECOND_IS_NULL
                };
            }
            else if (x != null && y == null)
            {
                result = new SimpleCompareResult { Result = 0 };
            }
            else
            {
                result = _innerComparer.Compare(x, y);
            }

            return result;
        }
    }
}
