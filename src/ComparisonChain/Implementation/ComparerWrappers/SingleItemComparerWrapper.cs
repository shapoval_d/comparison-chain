﻿using System.Collections.Generic;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation.CompareResults;

namespace ComparisonChain.Implementation.ComparerWrappers
{
    /// <summary>
    /// Provides the way to compare TResult objects
    /// </summary>
    /// <typeparam name="TResult">Type of compared objects</typeparam>
    internal class SingleItemComparerWrapper<TResult> : IComparerWrapper<TResult>
    {
        private readonly IComparer<TResult> _comparer;

        /// <summary>
        /// Creates an instance of SingleItemComparerWrapper class
        /// </summary>
        /// <param name="comparer">Comparer, which will be used to compare TResult objects</param>
        internal SingleItemComparerWrapper(IComparer<TResult> comparer)
        {
            this._comparer = comparer;
        }

        /// <summary>
        /// Returns result of compare of two TResult objects
        /// </summary>
        /// <param name="x">First of compared</param>
        /// <param name="y">Second of compared</param>
        /// <returns>Result of compare of two TResult objects</returns>
        public ICompareResult Compare(TResult x, TResult y)
        {
            var result = new SimpleCompareResult();
            if (_comparer is ComparisonChain<TResult>)
            {
                var internalResult = ((ComparisonChain<TResult>)_comparer).InternalCompare(x, y);
                if (internalResult.Result != 0)
                {
                    result.Result = internalResult.Result;
                    result.InnerFaults = internalResult.FaultyResults;
                }
            }
            else
            {
                var internalResult = _comparer.Compare(x, y);
                if (internalResult != 0)
                {
                    result.Result = internalResult;
                    result.Message = string.Format("({0};{1})", x.ToString(), y.ToString());
                }
            }

            return result;
        }
    }
}
