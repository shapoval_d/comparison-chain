﻿using System.Collections.Generic;
using ComparisonChain.Contracts;
using ComparisonChain.Implementation.CompareResults;

namespace ComparisonChain.Implementation.ComparerWrappers
{
    /// <summary>
    /// Provides the way to compare IEnumerable of TResult collections
    /// </summary>
    /// <typeparam name="TResult">Type of items in the compared collections</typeparam>
    internal class EnumerableComparerWrapper<TResult> : IComparerWrapper<IEnumerable<TResult>>
    {
        private readonly IComparerWrapper<TResult> _itemComparer;

        /// <summary>
        /// Creates an instance of EnumerableComparerWrapper class
        /// </summary>
        /// <param name="comparer">Comparer, which will be used to compare items in IEnumerable of TResult collections</param>
        internal EnumerableComparerWrapper(IComparerWrapper<TResult> comparer)
        {
            this._itemComparer = comparer;
        }

        /// <summary>
        /// Returns comparison result of two collection of TResult objects
        /// </summary>
        /// <param name="x">First of compared</param>
        /// <param name="y">Second of compared</param>
        /// <returns>Result of compare of two TResult objects</returns>
        public ICompareResult Compare(IEnumerable<TResult> x, IEnumerable<TResult> y)
        {
            CompareResultBase result = null;
            if (x == null && y != null)
            {
                result = new SimpleCompareResult
                {
                    Result = -1,
                    Message = ComparisonChainConstants.FIRST_IS_NULL
                };
            }
            else if (x != null && y == null)
            {
                result = new SimpleCompareResult
                {
                    Result = 1,
                    Message = ComparisonChainConstants.SECOND_IS_NULL
                };
            }
            else if (x == null && y == null)
            {
                result = new SimpleCompareResult();
            }
            else
            {
                result = ItemsCompare(x, y);
            }

            return result;
        }

        private CompareResultBase ItemsCompare(IEnumerable<TResult> x, IEnumerable<TResult> y)
        {
            var compareResult = new CollectionCompareResult();

            var xEnumerator = x.GetEnumerator();
            var yEnumerator = y.GetEnumerator();

            int i = 0;

            while (true)
            {
                var xCouldMove = xEnumerator.MoveNext();
                var yCouldMove = yEnumerator.MoveNext();

                if (xCouldMove != yCouldMove)
                {
                    var currentCount = (i + 1).ToString();
                    return new SimpleCompareResult
                    {
                        Result = xCouldMove ? 1 : -1,
                        Message = ComparisonChainConstants.DIFFERENT_COUNT_OF_ITEMS
                    };
                }

                if (!xCouldMove)
                {
                    break;
                }

                var innerResult = _itemComparer.Compare(xEnumerator.Current, yEnumerator.Current);
                compareResult.AddResult(i, (SimpleCompareResult)innerResult);
                i++;
            }

            return compareResult;
        }
    }
}
