﻿using ComparisonChain.Contracts;
using ComparisonChain.Implementation.CompareResults;

namespace ComparisonChain.Implementation.ComparisonLinks
{
    /// <summary>
    /// Provides the way to compare properties in T objects
    /// </summary>
    /// <typeparam name="T">Type of object which properties will be compared</typeparam>
    /// <typeparam name="TResult">Type of compared properties in T objects</typeparam>
    internal class SingleItemComparisonLink<T, TResult> : IComparisonLink<T>
    {
        private IComparerWrapper<TResult> _comparer;
        private IFunctorWrapper<T, TResult> _functor;

        /// <summary>
        /// Creates an instance of SingleItemComparisonLink class
        /// </summary>
        /// <param name="functor">Function, which provides property value from the compared objects</param>
        /// <param name="comparer">Comparer which will be used to compare values in the specified property</param>
        internal SingleItemComparisonLink(IFunctorWrapper<T, TResult> functor, IComparerWrapper<TResult> comparer)
        {
            this._functor = functor;
            this._comparer = comparer;
        }

        /// <summary>
        /// Returns result of compare of TResult properties in two T objects
        /// </summary>
        /// <param name="x">First of compared</param>
        /// <param name="y">Second of compared</param>
        /// <returns>Result of compare of two T objects</returns>
        public ICompareResult Compare(T x, T y)
        {
            var firstProperty = _functor.GetPropertyValue(x);
            var secondProperty = _functor.GetPropertyValue(y);

            var result = _comparer.Compare(firstProperty, secondProperty);
            if (result.Result != 0)
            {
                result.PropertyPath = this._functor.GetPropertyPath();
            }

            return result;
        }
    }
}
