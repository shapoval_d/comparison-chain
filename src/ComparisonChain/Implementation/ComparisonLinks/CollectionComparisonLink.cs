﻿using System.Collections.Generic;
using ComparisonChain.Contracts;

namespace ComparisonChain.Implementation.ComparisonLinks
{
    /// <summary>
    /// Provides the way to compare collection properties in T objects
    /// </summary>
    /// <typeparam name="T">Type of object which properties will be compared</typeparam>
    /// <typeparam name="TResult">Type of items in collections in the compared properties in T objects</typeparam>
    internal class CollectionComparisonLink<T, TResult> : IComparisonLink<T>
    {
        private IComparerWrapper<IEnumerable<TResult>> _comparer;
        private readonly IFunctorWrapper<T, IEnumerable<TResult>> _functor;

        /// <summary>
        /// Creates an instance of CollectionComparisonLink class
        /// </summary>
        /// <param name="functor">Function, which provides property value from the compared objects</param>
        /// <param name="comparer">Comparer which will be used to compare collections in the specified collection-based property</param>
        internal CollectionComparisonLink(IFunctorWrapper<T, IEnumerable<TResult>> functor, IComparerWrapper<IEnumerable<TResult>> comparer)
        {
            this._functor = functor;
            this._comparer = comparer;
        }

        /// <summary>
        /// Returns comparison result of TResult collection properties in two T objects
        /// </summary>
        /// <param name="x">First of compared</param>
        /// <param name="y">Second of compared</param>
        /// <returns>Result of compare of two T objects</returns>
        public ICompareResult Compare(T x, T y)
        {
            var firstProperty = _functor.GetPropertyValue(x);
            var secondProperty = _functor.GetPropertyValue(y);

            var result = _comparer.Compare(firstProperty, secondProperty);
            if (result.Result != 0)
            {
                result.PropertyPath = this._functor.GetPropertyPath();
            }

            return result;
        }
    }
}
