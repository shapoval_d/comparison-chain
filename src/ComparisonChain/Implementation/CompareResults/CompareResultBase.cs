﻿using System;
using ComparisonChain.Contracts;

namespace ComparisonChain.Implementation.CompareResults
{
    /// <summary>
    /// Base clase for objects, which represent comparison result of property in two objects
    /// </summary>
    internal abstract class CompareResultBase : ICompareResult
    {
        /// <summary>
        /// Gets or sets number representation of comparison result of property in two compared objects
        /// A signed integer that indicates the relative values of two compared objects. Value Meaning Less than zero first of compared is less than second. Zero compared objects are equals. Greater than zero first of compare is greater than second.
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        /// Gets or sets path to compared property
        /// </summary>
        public string PropertyPath { get; set; }

        /// <summary>
        /// Returns string representation of faults occured during comparing objects
        /// </summary>
        /// <returns>String representation of faults occured during comparing objects</returns>
        public abstract string ShowFaults();
    }
}
