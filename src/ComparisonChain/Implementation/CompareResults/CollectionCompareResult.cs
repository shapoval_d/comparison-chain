﻿using System.Collections.Generic;
using System.Text;

namespace ComparisonChain.Implementation.CompareResults
{
    /// <summary>
    /// Represent comparison result of collection property
    /// </summary>
    internal class CollectionCompareResult : CompareResultBase
    {
        private readonly Dictionary<int, SimpleCompareResult> _innerResults = new Dictionary<int, SimpleCompareResult>();

        /// <summary>
        /// Processes comparison results of two items  
        /// </summary>
        /// <param name="index">Position of compared items</param>
        /// <param name="innerResult">Result of compare</param>
        internal void AddResult(int index, SimpleCompareResult innerResult)
        {
            if (innerResult.Result != 0)
            {
                this.Result = innerResult.Result;
                _innerResults.Add(index, innerResult);
            }            
        }

        /// <summary>
        /// Returns string representation of faults occured during comparing objects
        /// </summary> 
        /// <returns>String representation of faults occured during comparing objects</returns>
        public override string ShowFaults()
        {
            var result = new StringBuilder();
            if (!string.IsNullOrEmpty(this.PropertyPath))
            {
                result.AppendFormat("{0} : ", this.PropertyPath);
            }

            foreach (var item in _innerResults)
            {
                result.AppendFormat("\n{0}[{1}] => {2}",
                                    ComparisonChainConstants.SHIFT,
                                    item.Key.ToString(),
                                    item.Value.ShowFaults().Replace("\n", ComparisonChainConstants.NEWLINE_SHIFT));
            }

            return result.ToString();
        }
    }
}
