﻿using ComparisonChain.Contracts;
using System.Collections.Generic;
using System.Text;

namespace ComparisonChain.Implementation.CompareResults
{
    /// <summary>
    /// Represent comparison result of non-collection property
    /// </summary>
    internal class SimpleCompareResult : CompareResultBase
    {
        private IList<ICompareResult> _innerFaults;

        /// <summary>
        /// Gets or sets string representation of fault occured during comparing 
        /// </summary>
        internal string Message { get; set; }

        /// <summary>
        /// Gets or sets collection of inner faults occured during comparing
        /// </summary>
        internal IList<ICompareResult> InnerFaults
        {
            get
            {
                if (this._innerFaults == null)
                {
                    this._innerFaults = new List<ICompareResult>();
                }
                return this._innerFaults;
            }
            set
            {
                this._innerFaults = value;
            }
        }

        /// <summary>
        /// Returns string representation of faults occured during comparing objects
        /// </summary>
        /// <returns>String representation of faults occured during comparing objects</returns>
        public override string ShowFaults()
        {
            var result = new StringBuilder();
            if (!string.IsNullOrEmpty(this.PropertyPath))
            {
                result.AppendFormat("{0} : ", this.PropertyPath);
            }
            
            if (!string.IsNullOrEmpty(Message))
            {
                result.Append(Message);
            }

            if (_innerFaults != null)
            {
                foreach (var item in _innerFaults)
                {
                    result.AppendFormat("\n{0}{1}",
                                        ComparisonChainConstants.SHIFT,
                                        item.ShowFaults().Replace("\n", ComparisonChainConstants.NEWLINE_SHIFT));
                }
            }           

            return result.ToString();
        }
    }
}
