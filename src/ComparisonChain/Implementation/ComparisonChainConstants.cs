﻿namespace ComparisonChain.Implementation
{
    internal static class ComparisonChainConstants
    {
        internal const string FIRST_IS_NULL = "First parameter is Null!";
        internal const string SECOND_IS_NULL = "Second parameter is Null!";
        internal const string SHIFT = "    ";
        internal const string DOUBLE_SHIFT = "        ";
        internal const string NEWLINE_SHIFT = "\n    ";
        internal const string DIFFERENT_COUNT_OF_ITEMS = "Different count of items";
    }
}
