﻿using System.Collections.Generic;

namespace ComparisonChain.Contracts
{
    public interface IComparisonChain<in T> : IComparer<T>
    {
    }
}
