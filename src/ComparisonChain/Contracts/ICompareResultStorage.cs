﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComparisonChain.Contracts
{
    public interface ICompareResultStorage
    {
        /// <summary>
        /// Gets collection of faulted compare results
        /// </summary>    
        IList<ICompareResult> FaultyResults { get; }

        /// <summary>
        /// Gets or sets number representation of comparison result of property in two compared objects
        /// A signed integer that indicates the relative values of two compared objects. Value Meaning Less than zero first of compared is less than second. Zero compared objects are equals. Greater than zero first of compare is greater than second.
        /// </summary>
        int Result { get; }

        /// <summary>
        /// Processes internal comparison result  
        /// </summary>
        /// <param name="internalResult">Result of compare</param>
        void AddCompareResult(ICompareResult internalResult);

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        string ShowFaults();
    }
}
