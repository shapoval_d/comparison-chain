﻿namespace ComparisonChain.Contracts
{
    public interface IComparerWrapper<in TResult>
    {
        /// <summary>
        /// Returns result of compare of two TResult objects
        /// </summary>
        /// <param name="x">First of compared</param>
        /// <param name="y">Second of compared</param>
        /// <returns>Result of compare of two TResult objects</returns>
        ICompareResult Compare(TResult x, TResult y);
    }
}
