﻿namespace ComparisonChain.Contracts
{
    internal interface IComparisonLink<T>
    {
        /// <summary>
        /// Returns result of compare of two T objects
        /// </summary>
        /// <param name="x">First of compared</param>
        /// <param name="y">Second of compared</param>
        /// <returns>Result of compare of two T objects</returns>
        ICompareResult Compare(T x, T y);
    }
}
