﻿using System.Collections.Generic;

namespace ComparisonChain.Contracts
{
    public interface IComparerProvider
    {
        /// <summary>
        /// Provides comparer wrapper, which allow to compare T objects
        /// </summary>
        /// <typeparam name="T">Type of objects which will be compared by objects</typeparam>
        /// <param name="comparer">Comparer of TResult objects</param>
        /// <returns></returns>
        IComparerWrapper<T> GetComparer<T>(IComparer<T> comparer);
        
        /// <summary>
        /// Provides comparer wrapper, which allow to compare collections of T objects
        /// </summary>
        /// <typeparam name="T">Type of collection items which will be compared by objects</typeparam>
        /// <param name="comparer">Comparer of TResult objects</param>
        /// <returns></returns>
        IComparerWrapper<IEnumerable<T>> GetCollectionComparer<T>(IComparer<T> comparer);

        /// <summary>
        /// Provides comparer wrapper in compare-runtime
        /// </summary>
        /// <typeparam name="T">Type of objects which will be compared by objects</typeparam>
        /// <returns>Comparer of TResult objects</returns>
        IComparerWrapper<T> GetRuntimeComparer<T>();

        /// <summary>
        /// Registers comparer for TResult object
        /// </summary>
        /// <typeparam name="TResult">Type of objects which will be compared by specified comparer</typeparam>
        /// <param name="comparer">Comparer which will be used to compare TResult objects</param>
        void RegisterComparer<TResult>(IComparer<TResult> comparer);
    }
}
