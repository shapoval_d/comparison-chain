﻿namespace ComparisonChain.Contracts
{
    public interface IFunctorWrapper<in T, out TResult>
    {
        /// <summary>
        /// Returns value of the specified property from the source object 
        /// </summary>
        /// <param name="sourceObject">Object which property value is required</param>
        /// <returns>Property value</returns>
        TResult GetPropertyValue(T sourceObject);

        /// <summary>
        /// Returns path to compared property
        /// </summary>
        /// <returns>Path to compared property</returns>
        string GetPropertyPath();
    }
}